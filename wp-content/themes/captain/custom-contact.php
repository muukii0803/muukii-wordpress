<?php
/*

Template Name: Contact

*/

get_header();

$latitude = of_get_option("contact_map_lat");
$longitude = of_get_option("contact_map_lon");
$map_show = of_get_option("contact_map_show");

if (have_posts()) : while (have_posts()) : the_post();

$page_title = get_post_meta(get_the_ID(), 'si_page_title', true);
$page_icon = get_post_meta(get_the_ID(), 'si_page_icon', true);

?>

<div id="page-<?php the_ID(); ?>" class="section fullwidth">

	<!-- #crumbs -->
	<div id="crumbs">
	<?php if(function_exists("bcn_display")) :
        bcn_display();
    endif; ?>
    </div>

	<h1 id="page_title"><?php the_title(); ?></h1>
	
	<?php if ($map_show == "1") : ?>
	<div id="map">
	
		<div id="map-canvas" data-zoom="10" data-cord="<?php if ($latitude != "") { echo $latitude; } else { echo "40.690371"; } ?>;<?php if ($longitude != "") { echo $longitude; } else { echo "-74.044491"; } ?>"></div> <?php /* !!! THEME OPTIONS !!!*/?>
		
		<?php shorti_map_script(); ?>
		
	</div>
	<?php endif; ?>
	
	<div class="one-half">
		
		<?php the_content(); ?>

	</div>
	
	<div class="one-half column-last">
	
		<!-- BEGIN #contact_form -->
		<form id="contact_form" onsubmit="return validate_form()" method="post">
		
			<p>
				<label><?php _e("Name", "shorti"); ?></label>
				<input onfocus="jQuery('#name_error').fadeOut('fast');" onclick="jQuery('#name_error').fadeOut('fast');" type ="text" name="name" id="name" />
			</p>
       
			
			<p>
				<label><?php _e("Email", "shorti"); ?></label>
				<input onfocus="jQuery('#email_error').fadeOut('fast');" onclick="jQuery('#email_error').fadeOut('fast');" type="text" name="email" id="email" />
			</p>
       
			<p>
				<label><?php _e("Message", "shorti"); ?></label>
				<textarea onfocus="jQuery('#message_error').fadeOut('fast');" onclick="jQuery('#message_error').fadeOut('fast');" id="message" rows="8" cols ="20"></textarea>
			</p>
	       
	       <button type="submit" name="submit" id="submit_button" class="btn"><?php _e("Send Message", "shorti"); ?><i class="icon-envelope-alt"></i></button>
		       
		</form>
		
		<?php shorti_ajax_contact(); ?>
		<!-- END #contact_form -->
	
	</div>
	
	<div class="clearfix"></div>

</div>

<?php

endwhile; endif; wp_reset_query();
get_footer();

?>