<?php get_header(); ?>

	<div id="posts" class="section">
	
		<?php
		
		$paged = 1;
		if ( get_query_var('paged') ) $paged = get_query_var('paged');
		if ( get_query_var('page') ) $paged = get_query_var('page');
        query_posts("ignore_sticky_posts=1&paged=".$paged);
		if (have_posts()) : while (have_posts()) : the_post();
		
		?>
		
		<article id="post_<?php the_ID(); ?>" <?php post_class("padL"); ?>>
		
			<?php
		
			$format = get_post_format();
			get_template_part( "formats/".$format );
			
			if($format == "")
			get_template_part( "formats/standard" );
			
			?>
			
		</article>
		
		<?php shorti_sticky_scroll(); ?>
			
		<?php endwhile; ?>
		
		<!--=== Begin Post Navigation ===-->
		<div id="post_nav" class="padL left_side">
			<div class="alignleft older"><?php next_posts_link( __("&laquo; Older Posts", "shorti") ) ?></div>
			<div class="alignright newer"><?php previous_posts_link( __("Newer Posts &raquo;", "shorti") ) ?></div>
			<?php wp_link_pages(); ?>
		</div>
		<!--=== End Post Navigation ===-->
		
		<div class="clearfix"></div>
	
	</div>
	
	<?php endif; wp_reset_query(); ?>
				
<?php get_footer(); ?>