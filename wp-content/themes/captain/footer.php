		
			<!--// BEGIN #footer //-->
			<footer id="footer">
			
				<p class="copy">&copy; <?php bloginfo("name"); ?>. <?php echo of_get_option("footer_copy"); ?></p>
			
			</footer>
			<!--// END #footer //-->
			
			<div class="clearfix"></div>
		
		</div>
		
		<div class="clearfix"></div>
		
	</section>
	<!--// BEGIN #main //-->
	
	
	
	<!--// BEGIN #meta //-->
	<section id="meta"></section>
	<!--// END #meta //-->
	

	
	<!--// BEGIN JS Settings //-->
	<?php get_template_part( "includes/settings-js" ); ?>
	<!--// END JS Settings //-->
	
	
	
	<?php get_template_part( "includes/background" ); ?>



<?php wp_footer(); ?>

</body>

</html>