/*================================================================================================================

	Custom Settings

================================================================================================================*/



jQuery.noConflict();



/* After DOM is Initialized */

jQuery(document).ready(function(){



	"use strict";
	jQuery("#control #side").nanoScroller({
		contentClass: 'side-inner',
		preventPageScrolling: 'true'
	});
	
	jQuery("#control .side").nanoScroller({
		contentClass: 'side-inner',
		preventPageScrolling: 'true'
	});
	
	
	
	jQuery(".close").click(function() {
    
        jQuery(".project_load").slideUp("slow");
    
    });
	
	
	
	// Smoothscroll
	
		jQuery('a[href^="#"]').not(".filter a").bind('click.smoothscroll',function (e) {
		
		    e.preventDefault();
		 
		    var target = this.hash,
		    $target = jQuery(target);
		 
		    jQuery('html, body').stop().animate({
		    
		        'scrollTop': $target.offset().top
		        
		    }, 900, 'easeInOutCirc', function () {
		    
		        window.location.hash = target;
		        
		    });
		    
		});



	// Variables
	
	var body = jQuery("body");
	
	

	// Responsive
	
		var screenWidth = body.width();
		
		if (screenWidth > 880) {
			
			jQuery("#control li, .post_title").not("#nav .current_page_item, #nav .current-menu-item, .single .post_title, .shorti_social li").hover(
			
				function()
			    {
			        jQuery(this).stop(true,true).animate({marginLeft: "5px"}, 300);
			    },
			    function()
			    {
			        jQuery(this).stop(true,true).animate({marginLeft: "0"}, 300);
			    }
			
			);
			
			jQuery(".post_more").hover(
			
				function()
			    {
			        jQuery(this).find("i").stop(true,true).animate({marginLeft: "5px"}, 300);
			    },
			    function()
			    {
			        jQuery(this).find("i").stop(true,true).animate({marginLeft: "0"}, 300);
			    }
			
			);
			
			jQuery(".post_back").hover(
			
				function()
			    {
			        jQuery(this).find("i").stop(true,true).animate({marginLeft: "-5px"}, 300);
			    },
			    function()
			    {
			        jQuery(this).find("i").stop(true,true).animate({marginLeft: "0"}, 300);
			    }
			
			);
		
			/*jQuery("#control").animate({width: "27%"}, 700, 'easeInOutCirc').find("#side").delay(700).fadeIn(700, 'easeInOutCirc');
			jQuery("#meta").animate({width: "13%"}, 700, 'easeInOutCirc');
			
			jQuery("#main").find("#content").delay(700).fadeIn(700, 'easeInOutCirc');*/
			
		}

	
	
	// Form Label Hide
	
		/* Focus & Blur */
			
			jQuery("#contact_form p input, #contact_form p textarea, #commentform input, #commentform textarea").focus(function() {
				jQuery(this).css({color: "#ffffff"});
			    jQuery(this).parent().find("> label").fadeOut(200);
			});
			
			jQuery("#contact_form p input, #contact_form p textarea, #commentform input, #commentform textarea").blur(function() {
				jQuery(this).css({color: "#323232"});
			    jQuery(this).parent().find("> label").fadeIn(200).css({color: "#a7abad"});
			});
		
		/* Empty & Full */
		
			var input = jQuery("#contact_form p input, #contact_form p textarea, #commentform input, #commentform textarea");
			
			input.blur(function() {
			
				if ( jQuery(this).val() != '') {
				
					
					    jQuery(this).parent().find("> label").hide();
				
				} else {
				
					    jQuery(this).parent().find("> label").show();
				
				}
			
			});
	
	
	
	// PrettyPhoto
	
		jQuery(".pretty").prettyPhoto({
		
			animation_speed:"fast",
			slideshow:7000,
			social_tools: false
			
		});
	
	
	
	// Sub Menus
		
		jQuery("#side #nav ul li").hover(
		    function()
		    {
		        jQuery(this).find("ul").stop(true,true).slideDown(200);
		    },
		    function()
		    {
		        jQuery(this).find("ul").stop(true,true).slideUp(800);
		    }
		);
		
	
	
	// Accordion
	jQuery(".accordion").accordion({ 
		animated: "slide",
		speed: "fast",
		autoHeight: false, 
		collapsible: true, 
		event: "click"
	});
	
	
	
	// jQuery Tabs
	jQuery(".tabs").tabs({
		/*fx: {
			height: "toggle",
			opacity: "toggle"
		},*/
		selected: 0
	});
	
	
	
});



/* After page is loaded */

jQuery(window).load(function(){



	// Projects - Isotope
	
		jQuery(function(){
	
		    var $projects = jQuery('#projects');
	
			$projects.imagesLoaded(function(){
				$projects.isotope({
					itemSelector : '.project',
					resizable: false,
					masonry: { columnWidth: $projects.width() / 3 }
				});
			});
			
			$projects.infinitescroll({
			
					navSelector  : '#project_nav',
					nextSelector : '#project_nav a',
					itemSelector : '.project',
					loading: {
						finishedMsg: 'No more projects.',
						msgText: "Loading more projects ...",
						img: 'wp-content/themes/flashback/images/loader.gif'
					}
					
				},
	
				function( newElements ) {
				
					var $newElems = jQuery( newElements ).css({ opacity: 0 });
					
					$newElems.imagesLoaded(function(){
					
						$newElems.animate({ opacity: 1 });
						$projects.isotope( 'appended', $newElems, true ); 
						
						// Project Overlay Title
		
							jQuery(".project_overlay").each(function() {
							
								var overlay = jQuery(this).parent();
								
									tl = jQuery(this).find(".tl");
									tlH = tl.height();
									overlayH = overlay.height();
									tlM = (overlayH / 2) - (11);
							
								tl.css({marginTop: tlM + "px" });
							
							});
					
						// Project Overlays
					
							jQuery(".project").hover(
								function()
								{
									jQuery(this).find(".project_overlay").stop(true,true).fadeTo(300, 1);
								},
								function()
								{
									jQuery(this).find(".project_overlay").stop(true,true).fadeTo(700, 0);
								}
							);
						
					});
				
				}
		      
		    );
		    
		    // update columnWidth on window resize
				jQuery(window).smartresize(function(){
				   $projects.isotope({
				     resizable: false, // disable normal resizing
				     masonry: { columnWidth: $projects.width() / 3 }
				   });
				   // trigger resize to trigger isotope
				}).smartresize();
				
				// trigger isotope again after images have loaded
				$projects.imagesLoaded( function(){
				  jQuery(window).smartresize();
				});
		    
		    // filter isotope items
		    
				jQuery('.filter li a, #filter_drop option').not("#shuffle").click(function(){
					
					jQuery('.filter li a, #filter_drop option').removeClass('active');
					jQuery(this).addClass('active');
					
					var selector = jQuery(this).attr('data-filter');
					$projects.isotope({ filter: selector });
					
					return false;
					
				});
				
			// select filter
			
				jQuery("#filter_drop").change(function() {
			        var filters = jQuery(this).val();
			        $projects.isotope({
			            filter: filters
			        });
			    });
				
			// shuffle isotope items
				
				jQuery('#shuffle').click(function(){
			
					$projects.isotope('shuffle');
					
				});
		
		});
		
		// Project Overlay Title
		
			jQuery(".project_overlay").each(function() {
			
				var overlay = jQuery(this).parent();
				
					tl = jQuery(this).find(".tl");
					tlH = tl.height();
					overlayH = overlay.height();
					tlM = (overlayH / 2) - (11);
			
				tl.css({marginTop: tlM + "px" });
			
			});
	
		// Project Overlays
	
			jQuery(".project").hover(
				function()
				{
					jQuery(this).find(".project_overlay").stop(true,true).fadeTo(300, 1);
				},
				function()
				{
					jQuery(this).find(".project_overlay").stop(true,true).fadeTo(700, 0);
				}
			);
	
	

});