<?php

get_header();

if (have_posts()) : while (have_posts()) : the_post();

?>

<div id="page-<?php the_ID(); ?>" class="section fullwidth">

	<!-- #crumbs -->
	<div id="crumbs">
	<?php if(function_exists("bcn_display")) :
        bcn_display();
    endif; ?>
    </div>

	<h1 id="page_title"><?php the_title(); ?></h1>
	
	<?php the_content(); ?>
	
	<div class="clearfix"></div>

</div>

<?php

endwhile; endif; wp_reset_query();
get_footer();

?>