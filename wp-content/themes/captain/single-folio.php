<?php get_header(); ?>

<div class="section fullwidth">

<?php

if ( have_posts() ) : while ( have_posts() ) : the_post();

// Taxonomy
$cats = get_the_terms($post->ID, 'projects');
$count = count($cats);

// Meta
$url = get_post_meta(get_the_ID(), 'si_project_url', true);            

// Featured Image
$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'fullsize');

?>

	<div id="project-<?php the_ID(); ?>">
	
		<div class="one-half no-margin">
		
			<a href="<?php echo $img[0]; ?>" class="project_thumb pretty prettyPhoto[<?php the_ID(); ?>]"><img src="<?php echo $img[0]; ?>" alt="<?php the_title(); ?>" /></a>
		
		</div>
		
		<div class="one-half column-last no-margin">
		
			<h1 id="page_title"><?php the_title(); ?></h1>
			
			<div class="project_info">
			
				<p><strong><?php _e("Work", "shorti"); ?>:</strong>&nbsp; <?php echo shorti_taxonomies_terms_links(); ?> <br/> <strong><?php _e("Created", "shorti"); ?>:</strong>&nbsp; <?php the_date(); ?></p>
				
				<?php if ($url !="" ) : ?><p><strong><?php _e("Link", "shorti"); ?>:</strong>&nbsp; <a href="<?php echo $url; ?>"><?php echo $url; ?></a></p><?php endif; ?>
			
			</div>
			
			<?php the_content(); ?>
		
		</div>
	
	</div>
	
	<div class="clearfix"></div>
	
	<div class="none"><?php comment_form(); ?></div>

<?php endwhile; endif; ?> 

	<div class="clearfix"></div>

</div>

<?php get_footer(); ?>