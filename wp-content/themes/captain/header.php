<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!--===============================================-->
<!--//   Made By Shorti - www.shortithemes.com   //-->
<!--===============================================-->

<head>

	<!--=== Meta ===-->
	<meta http-equiv="Content-Type" content="<?php bloginfo("html_type"); ?>; charset=<?php bloginfo("charset"); ?>" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0" />
	
	<!--=== Title ===-->
	<title><?php wp_title("|", true, "right"); ?><?php bloginfo("name"); ?></title>
	
	<!--=== Pingback ===-->
	<link rel="pingback" href="<?php bloginfo( "pingback_url" ); ?>" />
	
	<!--// BEGIN CSS Settings //-->
	<?php get_template_part( "includes/settings-css" ); ?>
	<!--// END CSS Settings //-->
	
	<?php if (of_get_option("google_analytics") != "") : ?>
		<!--// BEGIN Google Analytics Code //-->
		<?php echo of_get_option("google_analytics"); ?>
		<!-- // END Google Analytics Code //-->
	<?php endif;  ?>
	
	<?php wp_head(); ?>
	<head>
<link rel="shortcut icon" href="./images/favicon.ico" />
</head>
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php 

// Logo & Sub Title
$logo = of_get_option("logo_reg");
$logo_w = of_get_option("logo_width");
$logo_h = of_get_option("logo_height");
$logo_retina = of_get_option("logo_ret");
$plain_text = of_get_option("plain_text");
$sub_title = of_get_option("sub_title");

?>



	<!--// BEGIN #control //-->
	<header id="control">
	
		<div id="c_bg"></div>
	
		<div id="side" class="side">
		
			<div class="side-inner">
			
				<!-- BEGIN #logo-->
				<div id="logo">
					
				<?php if ($plain_text == "0") : ?>
		
					<h1>
					
						<a href="<?php echo home_url("/") ?>">
						
							<?php if ($logo != "") : ?><img src="<?php echo $logo; ?>" alt="<?php bloginfo("name"); ?>" width="<?php echo $logo_w; ?>" height="<?php echo $logo_h; ?>" class="reg" /><?php endif; ?>
							<?php if ($logo_retina != "") : ?><img src="<?php echo $logo_retina; ?>" alt="<?php bloginfo("name"); ?>" width="<?php echo $logo_w; ?>" height="<?php echo $logo_h; ?>" class="ret" /><?php endif;?>
						
						</a>
					
					</h1>
				
				<?php elseif ($plain_text == "1" || $logo == "" ) : ?>
				
					<h1 class="plain_text"><a href="<?php echo home_url('/'); ?>"><?php bloginfo("name"); ?></a></h1>
				
				<?php endif; ?>
					
				<?php if ($sub_title != "") : ?>
				
					<h4 class="sub"><?php echo $sub_title; ?></h4>
				
				<?php else : ?>
				
					<h4 class="sub"><?php bloginfo("description"); ?></h4>
				
				<?php endif; ?>
				
				</div>
				<!-- END #logo -->
				
				<!-- BEGIN #nav -->
				<nav id="nav">
				
					<?php wp_nav_menu( array( "container" => false, "theme_location" => "main-menu", "menu_class" => "main-menu regular", "after" => "<span></span>", "link_before" => "<i class='icon-angle-right'></i>") ); ?>
					
					<div id="mobile_nav"><i class="icon-reorder"></i>
			
						<?php 
							
						$locations = get_nav_menu_locations();
						
						$menu = wp_get_nav_menu_object( $locations[ "main-menu" ] );
						
						$menu_items = wp_get_nav_menu_items( $menu->term_id );
						
						$menu_list = "<select onchange='location = this.options[this.selectedIndex].value;'>";
						
						$menu_list .= "<option>-------</option>";
						
						foreach ( ( array ) $menu_items as $key => $menu_item ) {
						   $title = $menu_item->title;
						   $url = $menu_item->url;
						   $menu_list .= "<option value='" . $url . "'>" . $title . "</option>";
						}
						$menu_list .= "</select>";
						
						echo $menu_list;
						
						?>
					
					</div>
				
				</nav>
				<!-- END #nav -->
			
				<?php get_sidebar(); ?>
				
				<div class="clearfix"></div>
			
			</div>
		
		</div>
	
	</header>
	<!--// END #control //-->
	
	
	
	<!--// BEGIN #main //-->
	<section id="main">
	
		<div id="content">
	