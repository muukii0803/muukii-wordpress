<?php
/*

Template Name: Archives

*/

get_header();

if (have_posts()) : while (have_posts()) : the_post();

?>

<div id="page-<?php the_ID(); ?>" class="section fullwidth">

	<!-- #crumbs -->
	<div id="crumbs">
	<?php if(function_exists("bcn_display")) :
        bcn_display();
    endif; ?>
    </div>
	
	<h1 id="page_title"><?php the_title(); ?></h1>
	
	<?php the_content(); ?>
	
	<div id="archives">
	
		<div class="one-third">
		
			<div class="bump">
            
	            <h4><?php _e("Latest Posts", "shorti"); ?></h4>
				
				<ul><?php wp_get_archives("type=postbypost&sort_column=menu_order&before=<i class='icon-angle-right'></i>"); ?></ul>
			
			</div>
		
		</div>
		
		<div class="one-third">
		
			<div class="bump">
			
				<h4><?php _e("Latest Projects", "shorti"); ?></h4>
				
				<ul class="all_projects">
				
				<?php
				query_posts("post_type=folio&orderby=date&posts_per_page=25");
	            if (have_posts()) : while (have_posts()) : the_post();
	            ?>
	            
		            <li><i class="icon-angle-right"></i><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	            
	            <?php endwhile; endif; wp_reset_query(); ?>
	            
	            </ul>
            
            </div>
            
            <div class="bump">
			
				<h4><?php _e("Pages", "shorti"); ?></h4>
			
				<ul><?php wp_list_pages("link_before=<i class='icon-angle-right'></i>&title_li="); ?></ul>
			
			</div>
		
		</div>
		
		<div class="one-third column-last">
			
			<div class="bump">
			
				<h4><?php _e("Posts By Month", "shorti"); ?></h4>
				
				<ul><?php wp_get_archives("before=<i class='icon-angle-right'></i>&type=monthly"); ?></ul>
			
			</div>
			
			<div class="bump">
			
				<h4><?php _e("Posts By Year", "shorti"); ?></h4>
				
				<ul><?php wp_get_archives("before=<i class='icon-angle-right'></i>&type=yearly"); ?></ul>
			
			</div>
			
		</div>
	
	</div>
	
	<div class="clearfix"></div>

</div>

<?php

endwhile; endif; wp_reset_query();
get_footer();

?>