<?php

/*header("HTTP/1.1 301 Moved Permanently");
header("Location: ".get_bloginfo('url'));

exit();*/

get_header(); ?>

<div id="page-<?php the_ID(); ?>" class="section fullwidth">

	<!-- #crumbs -->
	<div id="crumbs">
	<?php if(function_exists("bcn_display")) :
        bcn_display();
    endif; ?>
    </div>

	<h1 id="page_title"><?php _e("Page Doesn't Exist", "shorti"); ?></h1>
	
	<?php the_content(); ?>
	
	<p><?php _e("Hit the logo or the button below to navigate back home.", "shorti"); ?></p>
	
	<a href="<?php echo home_url("/"); ?>" class="btn"><?php _e("Return Home", "shorti"); ?></a>
	
	<div class="clearfix"></div>

</div>

<?php get_footer(); ?>