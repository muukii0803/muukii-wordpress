<?php
/*

Template Name: Portfolio

*/

get_header();

$shape = of_get_option("portfolio_shape");

$portfolio_title = of_get_option("portfolio_title");
$portfolio_subtitle = of_get_option("portfolio_subtitle");
$portfolio_count = of_get_option("portfolio_count");

if (have_posts()) : while (have_posts()) : the_post();

?>

<div id="page-<?php the_ID(); ?>" class="section fullwidth">

	<!-- #crumbs -->
	<div id="crumbs">
	<?php if(function_exists("bcn_display")) :
        bcn_display();
    endif; ?>
    </div>
	
	<h1 id="page_title"><?php the_title(); ?></h1>
	
	<?php the_content(); ?>
	
	<div id="portfolio">
	
		<?php if ($portfolio_title) : ?><h1 id="portfolio_title"><?php echo $portfolio_title; ?></h1><?php endif; ?>
		<?php if ($portfolio_subtitle) : ?><h5 id="portfolio_subtitle"><?php echo $portfolio_subtitle; ?></h5><?php endif; ?>
	
		<nav id="project_sort">
		
			<ul class="filter">

				<?php 
				
				echo "<li><a class='active btn' href='#' data-filter='*'>".__("All", "shorti")."</a></li>"; 
				
				$cats = get_terms('projects');
				
				foreach ($cats as $cat) {
				
				echo "<li><a data-filter='.".$cat->slug."' class='btn'>".$cat->name."</a></li>";
				
				} 
				
				echo "<li><a id='shuffle' class='btn'>".__("Shuffle", "shorti")."</a></li>"; 
				
				?>
			
			</ul>
			
			<?php shorti_select_filter(); ?>
			
		</nav>
		
		<div id="project_box">
	
			<a class="close" href="#main"><i class="icon-remove-circle"></i></a>
			<div class="project_load"></div>
		
		</div>
		
		<ul id="projects">
		
			<?php 
			
			$paged = 1;
			if ( get_query_var('paged') ) $paged = get_query_var('paged');
			if ( get_query_var('page') ) $paged = get_query_var('page');
	        query_posts("posts_per_page=".$portfolio_count."&ignore_sticky_posts=1&paged=".$paged."&post_type=folio");
			if (have_posts()) : while (have_posts()) : the_post();

            
            // Taxonomy
            $cats = get_the_terms($post->ID, 'projects');
            $count = count($cats);
            
            // Meta
            $url = get_post_meta(get_the_ID(), 'si_project_url', true);            
            
            // Featured Image
            $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'project_home');
            
			?>
			
			<li id="post-<?php the_ID(); ?>" class="project<?php if ( $count > 0 ) { foreach($cats as $category) { echo ' ' . $category->slug; } } ?><?php if ($shape == "circle") { ?> project_c<?php } elseif ($shape == "square") { ?> project_s<?php } ?>">
				
                <a href="#project_box" class="project_thumb"><img src="<?php echo $img[0]; ?>" alt="<?php the_title(); ?>" /></a>
                
	            <h2 class="project_title"><?php the_title(); ?></h2>
	            
	            <div class="project_links">
	            
		            <a href="#project_box" class="project_view"><i class="icon-eye-open"></i></a>
		            &bull;<a href="<?php echo $img[0]; ?>" title="<?php the_title(); ?>" class="project_enlarge pretty"><i class="icon-zoom-in"></i></a>
	            
	            </div>
	            
	            <script type="text/javascript">
            
	            jQuery("#post-<?php the_ID(); ?> .project_thumb, #post-<?php the_ID(); ?> .project_view").click( function(e) {
	            
		            jQuery(".close").fadeOut();
		            jQuery(".loader").fadeIn();
		            jQuery("#project_box").slideDown();
		            jQuery(".project_load").slideUp().delay(1000).load("<?php the_permalink(); ?> #project-<?php the_ID(); ?>", function() {
		            
			            jQuery(".close").fadeIn();
			            jQuery(".loader").fadeOut();
		            
		            }).slideDown();
		            
		            e.preventDefault();
	            
	            });
	            
	            jQuery(".close").click(function(){
		            
		            jQuery(this).fadeOut();
		            jQuery(".project_load").slideUp(500, function() {
					    jQuery(".project_load").empty();
					});
					jQuery("#project_box").delay(1000).slideUp();
	            
	            });
	            
	            </script>
                
            </li>
            
            <?php endwhile; ?>
            
        </ul>
        
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax_loader.gif" class="loader" alt="Ajax Loader" />
        
		<!--=== Begin Post Navigation ===-->
		<div id="project_nav">
			<div class="alignleft older pagenav"><?php next_posts_link( __("&laquo; Older Posts", "shorti"), 0 ) ?></div>
			<div class="alignright newer pagenav"><?php previous_posts_link( __("Newer Posts &raquo;", "shorti"), 0 ) ?></div>
			<?php wp_link_pages(); ?>
		</div>
		<!--=== End Post Navigation ===-->
        
        <?php endif; wp_reset_query(); ?>
	
	</div>
	
	<div class="clearfix"></div>
  	
</div>

<?php

endwhile; endif; wp_reset_query();
get_footer();

?>