<?php get_header(); ?>

<div id="page-<?php the_ID(); ?>" class="section fullwidth">

	<!-- #page_title -->
	<h1 id="page_title" class="underlined"><?php printf( __( 'Search Results found for "%s"', "shorti" ), "<span>" . get_search_query() . "</span>" ); ?></h1>
	
	<!--// BEGIN POSTS //-->
	<div class="clearfix">
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php
		
		$format = get_post_format();
		get_template_part( "formats/".$format );
		
		if($format == "")
		get_template_part( "formats/standard" );
		
		?>

	<?php endwhile; endif; wp_reset_query(); ?>
	
	<div class="clearfix"></div>

</div>

<?php get_footer(); ?>