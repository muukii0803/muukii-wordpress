<?php if (of_get_option("show_widgets") == "1") : ?>
<div id="widgets">

	<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('Sidebar') ) ?>

</div>
<?php endif; ?>