<?php

	$video_embed = get_post_meta(get_the_ID(), "si_video_embed", true);
	
?>

<div class="left_side">

	<div class="post_video">
	
		<div class="post_video_inner">

			<?php if($video_embed != "") : ?>
			
				<?php echo stripslashes(htmlspecialchars_decode($video_embed)); ?>
			
			<?php else: ?>
			
				<p><?php _e("Add Video", "shorti") ?></p>
				
			<?php endif; ?>
		
		</div>
	
	</div>
	
	<?php if (is_single()) :  ?>
	
		<h1 class="post_title"><?php the_title(); ?></h1>
	
		<div class="post_content">
		
			<?php the_content(); ?>
				
		</div>
		
		<?php if (of_get_option("show_social") == "1") : ?>
		
			<div class="post_social">
			
				<div class="fb-like" data-href="<?php the_permalink(); ?>" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false" data-font="arial"></div>
				
				<a href="https://twitter.com/share" class="twitter-share-button" data-via="meshorti" data-hashtags="<?php bloginfo("name"); ?>">Tweet</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			
			</div>
			
		<?php endif; ?>
		
		<?php comments_template('', true); ?>
		
	<?php else : ?>
	
		<h1 class="post_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		
		<div class="post_excerpt">
		
			<?php the_excerpt(); ?>
		
		</div>
		
		<?php if (of_get_option("show_social") == "1") : ?>
		
			<div class="post_social">
			
				<div class="fb-like" data-href="<?php the_permalink(); ?>" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false" data-font="arial"></div>
				
				<a href="https://twitter.com/share" class="twitter-share-button" data-via="meshorti" data-hashtags="<?php bloginfo("name"); ?>">Tweet</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			
			</div>
			
		<?php endif; ?>
	
	<?php endif; ?>
	
</div>

<div class="right_side">

	<div class="post_icon"><i class="icon-film"></i></div>
	
	<div class="post_date">
	
		<h2 class="dateM"><?php the_time("M"); ?></h2>
		<h1 class="dateD"><?php the_time("d"); ?></h1>
		<h2 class="dateY"><?php the_time("Y"); ?></h2>
	
	</div>
	
	<?php if (is_single()) : ?>
	
		<a href="<?php echo get_posts_page_url(); ?>" class="post_back"><?php _e("Back", "shorti"); ?> <br/><i class="icon-hand-left"></i></a>
	
	<?php else : ?>
	
		<a href="<?php the_permalink(); ?>" class="post_more"><?php _e("Read", "shorti"); ?> <br/><i class="icon-hand-right"></i></a>
	
	<?php endif; ?>
	
	<?php the_tags(''); ?>

</div>