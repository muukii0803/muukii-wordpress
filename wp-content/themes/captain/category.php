<?php get_header(); ?>
	
	<!-- #page_title -->
	<h1 id="page_title" class="underlined padL"><?php printf( __( "Category - %s", "shorti" ), "<span>" . single_cat_title( "", false ) . "</span>" ); ?></h1>
	
	<!--// BEGIN POSTS //-->
	<div class="clearfix">
	
	<?php while ( have_posts() ) : the_post();?>
	
	<article id="post_<?php the_ID(); ?>" <?php post_class("padL"); ?>>
	
		<?php
			
		$format = get_post_format();
		get_template_part( "formats/".$format );
		
		if($format == "")
		get_template_part( "formats/standard" );
		
		?>
		
	</article>
	
	<?php shorti_sticky_scroll(); ?>
	
	<?php endwhile; ?> 
	
	</div>
	<!--// BEGIN POSTS //-->
	
<?php get_sidebar(); ?>

<?php get_footer(); ?>