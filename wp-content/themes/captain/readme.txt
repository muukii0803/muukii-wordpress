= Captain WordPress Theme =

* Made by Shorti, http://demo.shortithemes.com/captain/

== ABOUT Captain ==

A powerful WordPress theme made for brave bloggers who share various types of information such as images, videos, audio, quotes, galleries, and more!