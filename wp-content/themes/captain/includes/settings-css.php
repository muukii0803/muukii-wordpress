<?php
$style_main_color = of_get_option("style_main_color");
$style_second_color = of_get_option("style_second_color");
$style_accent_color = of_get_option("style_accent_color");
$style_body_font = of_get_option("style_body_font");
$style_heading_font = of_get_option("style_heading_font");
$custom_styles = of_get_option("custom_styles");
?>


<style type="text/css">

/*=== Main Color ===*/

	a,
	.left_side h1,
	.left_side h2,
	.left_side h3,
	.left_side h4,
	.left_side h5,
	.left_side h6,
	.ui-accordion .ui-accordion-header {
		<?php if ($style_main_color != "") : ?>
		color: <?php echo $style_main_color; ?>;
		<?php else : ?>
		color: #2b373f;
		<?php endif; ?>
	}
		
	.btn,
	#control,
	.jp-type-single,	
	.empty_thumb,
	.flex-control-paging li a,
	.ui-accordion .ui-accordion-header.ui-accordion-header-active {
		<?php if ($style_main_color != "") : ?>
		background: <?php echo $style_main_color; ?>;
		<?php else : ?>
		background: #2b373f;
		<?php endif; ?>
	}
	.ui-accordion .ui-accordion-header.ui-accordion-header-active {
		<?php if ($style_main_color != "") : ?>
		background: <?php echo $style_main_color; ?> !important;
		<?php else : ?>
		background: #2b373f !important;
		<?php endif; ?>
	}
	
/*=== Accent Color ===*/

	.jp-play-bar,
	.jp-volume-bar-value,
	.flex-active {
		<?php if ($style_accent_color != "") : ?>
		background-color: <?php echo $style_accent_color; ?> !important;
		<?php else : ?>
		background-color: #ff4343 !important;
		<?php endif; ?>
	}
	
/*=== Secondary Color ===*/

	.sub,
	#mobile_nav,
	#side #nav ul li a,
	.widget .widget-title,
	.widget_recent_entries .post-date {
		<?php if ($style_second_color != "") : ?>
		color: <?php echo $style_second_color; ?>;
		<?php else : ?>
		color: #82909b;
		<?php endif; ?>
	}
	
/*=== Body Font ===*/
	
	body,
	h1 span,
	h5,
	.post .post_date h5,	
	#contact_form label,
	.widget_calendar #wp-calendar a {
		<?php if ($style_body_font == "opensans") : ?>
		font-family: "Open Sans", Helvetica, sans-serif;
		<?php elseif ($style_body_font == "sans") : ?>
		font-family: "Droid Sans", sans-serif;
		<?php elseif ($style_body_font == "serif") : ?>
		font-family: "Droid Serif", sans-serif;
		<?php endif; ?>
	}
	
	.widget .widget-title {
		<?php if ($style_body_font == "opensans") : ?>
		font-family: "Open Sans", Helvetica, sans-serif;
		<?php elseif ($style_body_font == "sans") : ?>
		font-family: "Droid Sans", sans-serif;
		<?php elseif ($style_body_font == "serif") : ?>
		font-family: "Droid Serif", sans-serif;
		<?php endif; ?>
	}
	
/*=== Heading Font ===*/

	h1, h2, h3, h5, h6,
	.sub,
	.filter a,
	.post .post_bottom .cats ul li a, 
	.format-link a {
		<?php if ($style_heading_font == "maven") : ?>
		font-family: "Maven Pro Light 200", sans-serif !important;
		<?php elseif ($style_heading_font == "opensanslight") : ?>
		font-family: "Open Sans", sans-serif !important;
		font-weight: 300 !important;
		<?php endif; ?>
	}
	
/*=== Custom CSS ===*/

	<?php echo $custom_styles; ?>

</style>