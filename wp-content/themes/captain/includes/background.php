<?php 
	
$duration = of_get_option("duration");
$fade = of_get_option("fade");

$back1 = of_get_option("back1");
$back2 = of_get_option("back2");
$back3 = of_get_option("back3");
$back4 = of_get_option("back4");
$back5 = of_get_option("back5");
$error_back = of_get_option("error_back");

?>

<!--// BEGIN Backstretch //-->
<script type="text/javascript">

jQuery(document).ready(function() {

	<?php if (is_404()) : ?>
	
		<?php if ($error_back != "") : ?>
	
		jQuery("#c_bg").backstretch("<?php echo $error_back; ?>");
		
		<?php else : ?>
		
		jQuery("#c_bg").backstretch([
		
			"<?php echo $back1; // First Image ?>",
			<?php if ($back2 != "") : ?>"<?php echo $back2; ?>",<?php endif; ?>
			<?php if ($back3 != "") : ?>"<?php echo $back3; ?>",<?php endif; ?>
			<?php if ($back4 != "") : ?>"<?php echo $back4; ?>",<?php endif; ?>
			<?php if ($back5 != "") : ?>"<?php echo $back5; ?>"<?php endif; ?>
			
		], {
		
			duration: <?php if ($duration != "") : ?><?php echo $duration; ?><?php else : ?>5000<?php endif; ?>,
			fade: <?php if ($fade != "") : ?><?php echo $fade; ?><?php else : ?>1000<?php endif; ?>
		
		});
		
		<?php endif; ?>
		
	<?php elseif (!is_single() || !is_page()) : ?>
	
		// Page Background
		
		<?php 
		
		global $wp_query;
		$thePostID = $wp_query->post->ID;
		$page_bg = get_post_meta($thePostID, 'si_page_bg', true);
		
		?>
		
		<?php if ($page_bg != "") : ?>
		
			jQuery("#c_bg").backstretch("<?php echo $page_bg; ?>");
		
		<?php else : ?>
		
			<?php if ($back1 == "") : ?>
			
			jQuery("#c_bg").backstretch("<?php echo get_stylesheet_directory_uri(); ?>/images/main_bg.jpg");
			
			<?php else : ?>
			
			jQuery("#c_bg").backstretch([
			
				"<?php echo $back1; // First Image ?>",
				<?php if ($back2 != "") : ?>"<?php echo $back2; ?>",<?php endif; ?>
				<?php if ($back3 != "") : ?>"<?php echo $back3; ?>",<?php endif; ?>
				<?php if ($back4 != "") : ?>"<?php echo $back4; ?>",<?php endif; ?>
				<?php if ($back5 != "") : ?>"<?php echo $back5; ?>"<?php endif; ?>
				
			], {
			
				duration: <?php if ($duration != "") : ?><?php echo $duration; ?><?php else : ?>5000<?php endif; ?>,
				fade: <?php if ($fade != "") : ?><?php echo $fade; ?><?php else : ?>1000<?php endif; ?>
			
			});
			
			<?php endif; ?>
		
		<?php endif; ?>
		
	<?php else : ?>
	
		jQuery("#c_bg").backstretch([
			
			"<?php echo $back1; // First Image ?>"
			<?php if ($back2 != "") : ?>,"<?php echo $back2; ?>"<?php endif; ?>
			<?php if ($back3 != "") : ?>,"<?php echo $back3; ?>"<?php endif; ?>
			<?php if ($back4 != "") : ?>,"<?php echo $back4; ?>"<?php endif; ?>
			<?php if ($back5 != "") : ?>,"<?php echo $back5; ?>"<?php endif; ?>
			
		], {
		
			duration: <?php if ($duration != "") : ?><?php echo $duration; ?><?php else : ?>5000<?php endif; ?>,
			fade: <?php if ($fade != "") : ?><?php echo $fade; ?><?php else : ?>1000<?php endif; ?>
		
		});
		
	<?php endif; ?>
	
});

</script>
<!--// END Backstretch //-->