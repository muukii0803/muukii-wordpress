<?php

$style_main_color = of_get_option("style_main_color");
$style_accent_color = of_get_option("style_accent_color");

?>

<script type="text/javascript">

jQuery.noConflict();

jQuery(window).load(function(){

// Hover

	jQuery("#side a, #logo h1").not(".btn, #nav a").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color: "<?php if ($style_accent_color != "") { echo $style_accent_color; } else { echo "#ff4343"; } ?>"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color:"#ffffff"}, 200);
	    }
	);
	
	jQuery("#nav a").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color: "<?php if ($style_accent_color != "") { echo $style_accent_color; } else { echo "#ff4343"; } ?>"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color:"#82909b"}, 200);
	    }
	);
	
	jQuery(".left_side a, #main .fullwidth a, #crumbs a").not(".btn, .jp-audio a, .flex-control-paging li a, .ui-tabs .ui-tabs-nav a").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color: "<?php if ($style_accent_color != "") { echo $style_accent_color; } else { echo "#ff4343"; } ?>"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color:"<?php if ($style_main_color != "") { echo $style_main_color; } else { echo "#2b373f"; } ?>"}, 200);
	    }
	);
	
	jQuery(".right_side a").not(".btn").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color: "<?php if ($style_accent_color != "") { echo $style_accent_color; } else { echo "#ff4343"; } ?>"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color:"#b8b8b8"}, 200);
	    }
	);
	
	jQuery("#mobile_nav").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color: "#ffffff"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({color:"#82909b"}, 200);
	    }
	);
	
// Button
	
	jQuery("#post_nav a").addClass("btn");
		
	jQuery(".btn, .flex-control-paging li a").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({backgroundColor: "<?php if ($style_accent_color != "") { echo $style_accent_color; } else { echo "#ff4343"; } ?>"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({backgroundColor:"<?php if ($style_main_color != "") { echo $style_main_color; } else { echo "#2b373f"; } ?>"}, 200);
	    }
	);
	
// Accordion

	jQuery(".ui-accordion .ui-accordion-header").hover(
	    function()
	    {
	        jQuery(this).stop(true,true).animate({backgroundColor: "<?php if ($style_main_color != "") { echo $style_main_color; } else { echo "#2b373f"; } ?>"}, 200);
	    },
	    function()
	    {
	        jQuery(this).stop(true,true).animate({backgroundColor:"#dddddd"}, 200);
	    }
	);
	
// Form Background Color
	
	jQuery("input[type=text], textarea").not("#searchform input").focus(function() {
	
	    jQuery(this).stop(true,true).animate({backgroundColor: "<?php if ($style_main_color != "") { echo $style_main_color; } else { echo "#2b373f"; } ?>"}, 200);
	    
	});
	
	jQuery("input[type=text], textarea").not("#searchform input").blur(function() {
	
	    jQuery(this).stop(true,true).animate({backgroundColor:"#f5f5f5"}, 200);
	    
	});

});

</script>