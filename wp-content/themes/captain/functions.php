<?php

/*===============================================================

		Functions

===============================================================*/



/*======= Setup =======*/

	add_action( 'after_setup_theme', 'si_setup' );
	
	if ( ! function_exists( 'si_setup' ) ):
	 
	function si_setup() {
	
		add_theme_support('post-thumbnails');
		add_theme_support( 'automatic-feed-links' );
		
	}
	
	endif;



/*======= Text Domain =======*/

	load_theme_textdomain('shorti');



/*======= Load From Functions Folder =======*/

	// Custom Functions
	include("functions/custom.php");
	// Posttypes
	include("functions/posttypes.php");
	// Shortcodes
	include("functions/shortcodes.php");
	// Theme Meta
	include("functions/meta-page.php");
	include("functions/meta-post.php");
	include("functions/meta-projects.php");
	// Widgets Areas
	include("functions/widget-areas.php");
	// Widgets
	include("functions/widgets.php");



/*======= Options Framework =======*/	

	if ( !function_exists( 'optionsframework_init' ) ) {
	
		/*-----------------------------------------------------------------------------------*/
		/* Options Framework Theme
		/*-----------------------------------------------------------------------------------*/
	
		/* Set the file path based on whether the Options Framework Theme is a parent theme or child theme */
	
		if ( get_template_directory() == get_template_directory() ) {
			define('OPTIONS_FRAMEWORK_URL', get_template_directory() . '/admin/');
			define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/admin/');
		} else {
			define('OPTIONS_FRAMEWORK_URL', get_template_directory() . '/admin/');
			define('OPTIONS_FRAMEWORK_DIRECTORY', get_stylesheet_directory_uri() . '/admin/');
		}
	
		require_once (OPTIONS_FRAMEWORK_URL . 'options-framework.php');
	
	}
	
	/* 
	 * This is an example of how to add custom scripts to the options panel.
	 * This example shows/hides an option when a checkbox is clicked.
	 */
	
	add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');
	
	function optionsframework_custom_scripts() { ?>
	
	<script type="text/javascript">
	jQuery(document).ready(function() {
	
		jQuery('#example_showhidden').click(function() {
	  		jQuery('#section-example_text_hidden').fadeToggle(400);
		});
		
		if (jQuery('#example_showhidden:checked').val() !== undefined) {
			jQuery('#section-example_text_hidden').show();
		}
		
	});
	</script>
	
	<?php }



/*======= Post Formats =======*/		

	add_theme_support( 'post-formats', array( 'image', 'gallery', 'video', 'audio', 'quote' ) );
	


/*======= Load JQuery =======*/	

	function si_enqueue_scripts() {
	
		if ( !is_admin() ) {
		
			wp_register_script('si_jplayer', get_template_directory_uri() . '/js/jquery.jplayer.min.js', 'jquery');
			wp_register_script('si_modernizr', get_template_directory_uri() . '/js/modernizr-transitions.js', 'jquery');
			wp_register_script('si_maps', 'http://maps.google.com/maps/api/js?sensor=false', 'jquery', '1', TRUE);
			wp_register_script('si_isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', 'jquery', '1', TRUE);
			wp_register_script('si_infinite', get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', 'jquery', '1', TRUE);
			wp_register_script('si_flex', get_template_directory_uri() . '/js/jquery.flexslider-min.js', 'jquery', '1', TRUE);
			wp_register_script('si_nano', get_template_directory_uri() . '/js/jquery.nanoscroller.min.js', 'jquery', '1', TRUE);
			wp_register_script('si_pretty', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', 'jquery', '1', TRUE);
			wp_register_script('si_back', get_template_directory_uri() . '/js/jquery.backstretch.min.js', 'jquery', '1', TRUE);
			wp_register_script('si_settings', get_template_directory_uri() . '/js/settings.js', 'jquery', '1', TRUE); 
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-tabs');
			wp_enqueue_script('jquery-ui-accordion');
			wp_enqueue_script('jquery-effects-core');
			wp_enqueue_script('jquery-effects-fade');
			wp_enqueue_script('si_jplayer');
			wp_enqueue_script('si_modernizr');
			wp_enqueue_script('si_isotope');
			wp_enqueue_script('si_maps');
			wp_enqueue_script('si_infinite');
			wp_enqueue_script('si_flex');
			wp_enqueue_script('si_nano');
			wp_enqueue_script('si_pretty');
			wp_enqueue_script('si_back');
			wp_enqueue_script('si_settings');
			
		}
	
	} 
	
	add_action('init', 'si_enqueue_scripts');
	
	
	
	// Meta Scripts
	function si_meta_scripts() {
	
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_register_script('si-upload', get_template_directory_uri() . '/functions/js/upload.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('si-upload');
		
	}
	
	add_action('admin_print_scripts', 'si_meta_scripts');
	
	
	
	// Meta Styles
	function si_meta_styles() {
	
		wp_enqueue_style('thickbox');
		
	}
	
	add_action('admin_print_styles', 'si_meta_styles');



/*======= Load Styles =======*/	

	function si_enqueue_styles() {
	
		if ( !is_admin() ) {
	
			wp_register_style('si_main_css', get_bloginfo( 'stylesheet_url' ));
			wp_register_style('si_pretty_css', get_template_directory_uri() . '/css/prettyPhoto.css');
			wp_register_style('si_flex_css', get_template_directory_uri() . '/css/flexslider.css');
			wp_register_style('si_respond_css', get_template_directory_uri() . '/css/responsive.css');
			wp_register_style('si_jplayer_css', get_template_directory_uri() . '/css/jplayer.css');
			wp_register_style('si_comments_css', get_template_directory_uri() . '/css/comments.css');
			wp_register_style('si_awesome_css', get_template_directory_uri() . '/css/font-awesome.min.css');
			
			wp_enqueue_style('si_main_css');
			wp_enqueue_style('si_pretty_css');
			wp_enqueue_style('si_flex_css');
			wp_enqueue_style('si_jplayer_css');
			wp_enqueue_style('si_respond_css');
			wp_enqueue_style('si_comments_css');
			wp_enqueue_style('si_awesome_css');
		
		}
				
	}
	
	add_action('init', 'si_enqueue_styles');
	
	
	
/*-----------------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ LOAD GOOGLE FONTS _-_-_-_-_-_-_-_-_-_-*/
/*-----------------------------------------------------------*/
	
    function si_enqueue_fonts() { 
    
        wp_register_style( 'si_droidserif', 'http://fonts.googleapis.com/css?family=Droid+Serif', array(), '', 'all' );
        wp_register_style( 'si_droidsans', 'http://fonts.googleapis.com/css?family=Droid+Sans', array(), '', 'all' );
        wp_register_style( 'si_opensans', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,300,400,600,700,800', array(), '', 'all' );
        wp_register_style( 'si_montserrat', 'http://fonts.googleapis.com/css?family=Montserrat:400,700', array(), '', 'all' );
        wp_register_style( 'si_francoisone', 'http://fonts.googleapis.com/css?family=Francois+One', array(), '', 'all' );
        wp_register_style( 'si_voltaire', 'http://fonts.googleapis.com/css?family=Voltaire', array(), '', 'all' );
        
        wp_enqueue_style( 'si_droidserif' );
		wp_enqueue_style( 'si_droidsans' );
		wp_enqueue_style( 'si_opensans' );
		wp_enqueue_style( 'si_montserrat' );
		wp_enqueue_style( 'si_francoisone' );
		wp_enqueue_style( 'si_voltaire' );
		        
    }
    
    add_action('wp_enqueue_scripts', 'si_enqueue_fonts');



/*======= Comment Reply Script =======*/	

	function si_comment_reply(){
	
		if (!is_admin()){
		
			if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script( 'comment-reply' );
			
		}
	  	
	}
	add_action('get_header', 'si_comment_reply');
	
/*----------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ THUMBNAILS _-_-_-_-_-_-_-_-_-_-*/
/*----------------------------------------------------*/	

	if ( function_exists( "add_theme_support" ) ) {
		add_theme_support( "post-thumbnails" );
		//add_image_size( "single-thumb", 660, "", true );
		add_image_size( "fullsize", "", "", true );
	}



/*======= Register Menu =======*/	

	function register_menu() {
		register_nav_menu('main-menu', __('Main Menu', 'shorti'));
	}
	add_action('init', 'register_menu');



/*======= Comments =======*/	

function si_comment($comment, $args, $depth) {

	$isByAuthor = false;
	
	if($comment->comment_author_email == get_the_author_meta('email')) {
	$isByAuthor = true;
	}
	
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	
	<div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
	
		<?php echo get_avatar($comment,$size='50'); ?>
		
		<div class="comment-author vcard">
		
			<?php if ($comment->user_id) {
			$user=get_userdata($comment->user_id);
			echo $user->display_name;
			} else { comment_author_link(); } ?>
			
			<?php if($isByAuthor) { ?><span class="author-tag"><?php _e('(Author)','shorti') ?></span><?php } ?>
		
		</div>
		
		<div class="comment-meta commentmetadata">
		
			<?php printf(__('%1$s at %2$s', 'shorti'), get_comment_date(),  get_comment_time()) ?>
			
			<?php edit_comment_link(__('(Edit)', 'shorti'),'  ','') ?> &middot; 
			
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			
		</div>
		
		<div class="comment-inner">
		
			<?php if ($comment->comment_approved == '0') : ?>
			<em class="moderation"><?php _e('Your comment is awaiting moderation.', 'shorti') ?></em>
			<br />
			<?php endif; ?>
			
			<?php comment_text() ?>
		
		</div>
	
	</div>

<?php
}



/*======= Posts Page URL =======*/	

function get_posts_page_url() {
	if( 'page' == get_option( 'show_on_front' ) ) {
		$posts_page_id = get_option( 'page_for_posts' );
		$posts_page = get_page( $posts_page_id );
		$posts_page_url = site_url( get_page_uri( $posts_page_id ) );
	}
	else {
		$posts_page_url = site_url();
	}
		return $posts_page_url;
}



/*======= Excerpt =======*/	

	function si_excerpt_length( $length ) {
		return 30;
	}
	add_filter( 'excerpt_length', 'si_excerpt_length' );
	
	function new_excerpt_more($excerpt) {
		return str_replace('[...]', '...', $excerpt);
	}
	add_filter('wp_trim_excerpt', 'new_excerpt_more');



/*======= Shortcode Menu =======*/	

	add_action('media_buttons','add_sc_select',11);
	function add_sc_select(){
	    global $shortcode_tags;
	     /* ------------------------------------- */
	     /* enter names of shortcode to exclude bellow */
	     /* ------------------------------------- */
	    $exclude = array("wp_caption", "embed", "caption", "gallery");
	    echo '&nbsp;<select id="sc_select"><option>Shortcode</option>';
	
	    foreach ($shortcode_tags as $key => $val){
		    if(!in_array($key,$exclude)){
	            $shortcodes_list .= '<option value="['.$key.'][/'.$key.']">'.$key.'</option>';
	    	    }
	        }
	     echo $shortcodes_list;
	     echo '</select>';
	    echo '
	    
	    <style type="text/css">
	    
	    .wp-media-buttons {
	    	padding-top: 0;
	    }
	    
	    </style>
	    
	    ';
	}
	add_action('admin_head', 'button_js');
	function button_js() {
		echo '<script type="text/javascript">
		jQuery(document).ready(function(){
		   jQuery("#sc_select").change(function() {
				  send_to_editor(jQuery("#sc_select :selected").val());
	        		  return false;
			});
		});
		</script>';
	}



/*======= Thumbnail Column =======*/
	
add_filter('manage_posts_columns', 'thumbnail_column');
function thumbnail_column($columns) {
    $column_thumbnail = array( 'thumbnail' => 'Thumbnail' );
	$columns = array_slice( $columns, 0, 2, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
    return $columns;
}

add_action('manage_posts_custom_column', 'show_thumbnail_column', 10, 1);
function show_thumbnail_column($name) {
    global $post;
    switch ($name) {
        case 'thumbnail':
            $thumbnail = get_the_post_thumbnail($post->ID);
            echo $thumbnail;
    }
}



/*======= Admin Menu Icons =======*/	
 
	add_action( 'admin_head', 'wpt_work_icons' );
	 
	function wpt_work_icons() {
	    ?>
		<style type="text/css" media="screen">
		
		/* Projects */
		
		#menu-posts-folio .wp-menu-image {
			background: url(<?php echo get_stylesheet_directory_uri() ?>/functions/images/work_menu_icon.png) top left no-repeat !important;
		}
		
		#menu-posts-folio:hover .wp-menu-image, #menu-posts-folio.wp-has-current-submenu .wp-menu-image {
			background-position: bottom left !important;
		}
		
		#icon-edit.icon32-posts-folio {
			background: url(<?php echo get_stylesheet_directory_uri() ?>/functions/images/work_icon_32.png) no-repeat;
		}
		
		/* Thumbnail Column */
		
		th#thumbnail { width: 100px }
		
		th#title { width: 150px }
		
		.thumbnail img { max-width: 100px; height: auto; }
		
		</style>
	<?php }



/*======= Required =======*/	

	if ( ! isset( $content_width ) )
		$content_width = 660;
		
		
		