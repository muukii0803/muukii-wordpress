<?php
/*=====================*/

// Sidebars & Widget Areas

/*=====================*/

if ( function_exists("register_sidebar") ) {

	// Main Sidebar
	register_sidebar(array(
		"name" => "Sidebar",
		"before_widget" => '<div id="%1$s" class="widget %2$s clearfix">',
		"after_widget" => "</div>",
		"before_title" => "<h4 class='widget-title'>",
		"after_title" => "</h4>",
	));

}