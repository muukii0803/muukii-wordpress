<?php

/*========================================================

	Social Icons - (by Shorti)

========================================================*/

add_action( 'widgets_init', 'shorti_social_widget' );


function shorti_social_widget() {

	register_widget( 'SHORTI_Social_Widget' );
	
}

class SHORTI_Social_Widget extends WP_Widget {

	function SHORTI_Social_Widget() {
	
		$widget_ops = array( 'classname' => 'shorti_social', 'description' => __('A robust custom social widget made by Shorti to display social icons.', 'shorti') );
		
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'shorti_social' );
		
		$this->WP_Widget( 'shorti_social', __('Social Icons - (by Shorti)', 'shorti'), $widget_ops, $control_ops );
		
	}
	
	function widget( $args, $instance ) {
	
		extract( $args );

		$title = apply_filters('widget_title', $instance['title'] );
		$facebook = $instance['facebook'];
		$twitter = $instance['twitter'];
		$googleplus = $instance['googleplus'];
		$pinterest = $instance['pinterest'];
		$linkedin = $instance['linkedin'];

		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;
			
		echo '<ul>';
			
		if ( $facebook )
			printf( '<li><a href="http://facebook.com/'.$facebook.'" target="_blank"><i class="icon-facebook-sign"></i></a></li>' );
			
		if ( $twitter )
			printf( '<li><a href="http://twitter.com/'.$twitter.'" target="_blank"><i class="icon-twitter-sign"></i></a></li>' );
			
		if ( $googleplus )
			printf( '<li><a href="https://plus.google.com/u/0/'.$googleplus.'/posts" target="_blank"><i class="icon-google-plus-sign"></i></a></li>' );
			
		if ( $pinterest )
			printf( '<li><a href="http://pinterest.com/'.$pinterest.'" target="_blank"><i class="icon-pinterest-sign"></i></a></li>' );
			
		if ( $linkedin )
			printf( '<li><a href="'.$linkedin.'" target="_blank"><i class="icon-linkedin-sign"></i></a></li>' );
			
		echo '</ul>';
		
		echo $after_widget;
		
	}

	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
	
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['facebook'] = strip_tags( $new_instance['facebook'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
		$instance['pinterest'] = strip_tags( $new_instance['pinterest'] );
		$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );

		return $instance;
		
	}

	
	function form( $instance ) {

		$defaults = array( 'title' => __('Social', 'shorti'), 'facebook' => __('', 'shorti'), 'twitter' => __('', 'shorti'), 'linkedin' => __('', 'shorti'), 'googleplus' => __('', 'shorti'), 'pinterest' => __('', 'shorti') );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'shorti'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e('Facebook ID:', 'shorti'); ?></label>
			<input id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php echo $instance['facebook']; ?>" class="widefat" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e('Twitter Username:', 'shorti'); ?></label>
			<input id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo $instance['twitter']; ?>" class="widefat" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'googleplus' ); ?>"><?php _e('Google Plus ID:', 'shorti'); ?></label>
			<input id="<?php echo $this->get_field_id( 'googleplus' ); ?>" name="<?php echo $this->get_field_name( 'googleplus' ); ?>" value="<?php echo $instance['googleplus']; ?>" class="widefat" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'pinterest' ); ?>"><?php _e('Pinterest Username:', 'shorti'); ?></label>
			<input id="<?php echo $this->get_field_id( 'pinterest' ); ?>" name="<?php echo $this->get_field_name( 'pinterest' ); ?>" value="<?php echo $instance['pinterest']; ?>" class="widefat" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'linkedin' ); ?>"><?php _e('LinkedIn Profile:', 'shorti'); ?></label>
			<input id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="<?php echo $instance['linkedin']; ?>" class="widefat" />
		</p>

	<?php
	
	}
	
}

?>