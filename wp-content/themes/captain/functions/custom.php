<?php



/*--------------------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ SHORTI SELECT FILTER _-_-_-_-_-_-_-_-_-_-*/
/*--------------------------------------------------------------*/

function shorti_select_filter() {

	$locations = get_nav_menu_locations();
				
	$menu = wp_get_nav_menu_object( $locations[ "main-menu" ] );
	
	$menu_items = wp_get_nav_menu_items( $menu->term_id );
	
	$menu_list = "<select id='filter_drop'>";
	
	$menu_list .= "<option value='*'>All</option>";
	
	$cats = get_terms('projects');
	
	foreach ($cats as $cat) {
	
	$menu_list .= "<option value='.".$cat->slug."'>".$cat->name."</option>";
	
	} 
	
	$menu_list .= "</select>";
	
	echo $menu_list;

}



/*-----------------------------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ SHORTI TAXONOMIES TERMS LINKS _-_-_-_-_-_-_-_-_-_-*/
/*-----------------------------------------------------------------------*/

function shorti_taxonomies_terms_links() {

	global $post, $post_id;

	$post = &get_post($post->ID);

	$post_type = $post->post_type;

	$taxonomies = get_object_taxonomies($post_type);
	
	foreach ($taxonomies as $taxonomy) {

		$terms = get_the_terms( $post->ID, $taxonomy );
		
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $term )
				//$out[] = '<a href="' .get_term_link($term->slug, $taxonomy) .'">'.$term->name.'</a>';
				$out[] = $term->name;
			$return = join( ', ', $out );
		}
		
	}
	
	return $return;
	
}



/*--------------------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ SHORTI STICKY SCROLL _-_-_-_-_-_-_-_-_-_-*/
/*--------------------------------------------------------------*/

function shorti_sticky_scroll() { ?>

	<script type="text/javascript">
		
	jQuery(window).bind( "scroll", function () {
		
		var screenWidth = jQuery("body").width();
		
		var ttop = jQuery('#post_<?php the_ID(); ?>').position().top;
		var hcltb = jQuery('#post_<?php the_ID(); ?>').height();
		var mtop = jQuery(window).scrollTop();
		
		if(mtop > ttop) {
		
			if (screenWidth > 880) {
			
				if (mtop < (ttop+hcltb-255)) {
					
					jQuery('#post_<?php the_ID(); ?> .right_side').stop(true).animate({top: (mtop-ttop+30)}, 500);
				
				}
				
			} else {
				
				if (mtop < (ttop+hcltb-135)) {
				
					jQuery('#post_<?php the_ID(); ?> .right_side').stop(true).animate({top: (mtop-ttop-80)}, 500);
				
				}
				
			}
			
		} else {
		
			jQuery('#post_<?php the_ID(); ?> .right_side').stop(true).animate({top: "0"});
		}		
		
	});
	
	</script>

<?php }



/*-------------------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ SHORTI AJAX CONTACT _-_-_-_-_-_-_-_-_-_-*/
/*-------------------------------------------------------------*/

function shorti_ajax_contact() { ?>

	<script type="text/javascript">

	function validate_form() {

        var error = 0;
        jQuery("#name_error").detach();
        jQuery("#email_error").detach();
        jQuery("#message_error").detach();
        
        

		// Name
        if (document.getElementById("name").value =="" || document.getElementById("name").value == null){
            jQuery("#name").animate({backgroundColor: "#ff4343"});
            jQuery("#name").parent().find("label").animate({color: "#fff"});
            error = 1;
        }
	        
	        
	    
		// Email
        var _regexp = new RegExp(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/);
        var _email = document.getElementById("email").value;
        if(_regexp.test(_email) == false) {
            jQuery("#email").animate({backgroundColor: "#ff4343"});
            jQuery("#email").parent().find("label").animate({color: "#fff"});
            error = 1;
        }
	        
	        
	    
		// Message
        if (document.getElementById("message").value =="" || document.getElementById("message").value == null){
            jQuery("#message").animate({backgroundColor: "#ff4343"});
            jQuery("#message").parent().find("label").animate({color: "#fff"});
            error = 1;
        }       
	 
        

        if (error == 1){
            return false;
        }else{
            var name = document.getElementById("name").value;
            var email = document.getElementById("email").value;
            var message = document.getElementById("message").value;
            jQuery("#loader").css("display","block");
            jQuery.ajax({
                type:       "POST",
                url:        "<?php echo get_stylesheet_directory_uri(); ?>/includes/contact.php",
                cache:      false,
                data:        "name=" + name +"&email="+email+"&message="+message,
                success:    function(html) {
                	jQuery("input[type=text], textarea").val("");
                    jQuery("#contact_form").before("<h3 class='center' id='send_message'>Message Sent!</h3>");
                    jQuery("#send_message").delay(1500).slideUp();
                }
            });
        }
	
	return false;
	
	}

	</script>

<?php }



/*-----------------------------------------------------------*/												
/*-_-_-_-_-_-_-_-_-_-_ SHORTI MAP SCRIPT _-_-_-_-_-_-_-_-_-_-*/
/*-----------------------------------------------------------*/

	function shorti_map_script() { ?>
	
	<script>

	jQuery(document).ready(function(){
	
		var body = jQuery("body");
	
		var cords = jQuery("#map-canvas").data('cord');
		var zoomi = jQuery("#map-canvas").data('zoom');
		var lines = cords.split(";");
		
		console.log(lines[0]);
		
		var map_options = {
			lat: lines[0],
			lon: lines[1],
			container_id: 'map-canvas',
			set_marker: true,
			zoom: zoomi,
			panControl: false,
            scrollwheel: false
		};
		
		initializeMap(map_options);
		
		function initializeMap(options) {
		
			var mapDiv = document.getElementById(options.container_id);
			var position = new google.maps.LatLng(options.lat, options.lon);
			
			var map = new google.maps.Map(mapDiv, {
				center: position,
				zoom: options.zoom,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				disableDefaultUI: true,
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.LARGE
				},
				mapTypeControlOptions: {
					style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				}
			});
			
			// Set marker if allowed
			if(options.set_marker)
			{
				new google.maps.Marker({map: map, position: position});
			}
			
			// For Purpose of Responsivity
			jQuery(window).bind('afterresize', function()
			{
				map.panTo(position);
			});
			
		}
	
	});
	
	</script>
	
<?php }


