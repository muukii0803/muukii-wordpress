<?php

/*===============================================================

// Shortcodes

===============================================================*/



/*========================================================

	Button

========================================================*/

function si_button( $atts, $content = null ) {
      
	extract(shortcode_atts(array(
		'url'    => '#',
		'icon'   => 'icon-arrow-right',
		'noicon' => '',
		'size' => ''
	), $atts));
	
	if ($noicon == '1') {
	
		$noicon = 'noicon';
	
	}
 
	return '<a href="'.$url.'" class="btn '.$size.'">' . do_shortcode($content) . '<i class="'.$icon.' '. $noicon .'"></i></a>';
   
}
add_shortcode( 'button', 'si_button' );



/*========================================================

	List

========================================================*/

function si_list( $atts, $content = null ) {
 
   return '<div class="check">' . do_shortcode($content) . '</div>';
   
}
add_shortcode( 'list', 'si_list' );



/*========================================================

	Title

========================================================*/

function si_title( $atts, $content = null ) {
 
   return '<h5>' . do_shortcode($content) . '</h5>';
   
}
add_shortcode( 'title', 'si_title' );



/*========================================================

	Full

========================================================*/

function si_full( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column full'.$margin.'">' . do_shortcode($content) . '</div>';
   
}
add_shortcode( 'full', 'si_full' );



/*========================================================

	One Half

========================================================*/

function si_one_half( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-half'.$margin.'">' . do_shortcode($content) . '</div>';
   
}
add_shortcode( 'one_half', 'si_one_half' );

function si_one_half_last( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-half column-last'.$margin.'">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
   
}
add_shortcode('one_half_last', 'si_one_half_last');



/*========================================================

	One Third

========================================================*/

function si_one_third( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-third'.$margin.'">' . do_shortcode($content) . '</div>';
   
}
add_shortcode( 'one_third', 'si_one_third' );

function si_one_third_last( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-third column-last'.$margin.'">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
   
}
add_shortcode('one_third_last', 'si_one_third_last');



/*========================================================

	One Fourth

========================================================*/

function si_one_fourth( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-fourth'.$margin.'">' . do_shortcode($content) . '</div>';
   
}
add_shortcode( 'one_fourth', 'si_one_fourth' );

function si_one_fourth_last( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-fourth column-last'.$margin.'">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
   
}
add_shortcode('one_fourth_last', 'si_one_fourth_last');



/*========================================================

	One Fifth

========================================================*/

function si_one_fifth( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-fifth'.$margin.'">' . do_shortcode($content) . '</div>';
   
}
add_shortcode( 'one_fifth', 'si_one_fifth' );

function si_one_fifth_last( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'margin'    => ''
	), $atts));
	
	if ($margin == '0') {
	
		$margin = ' no-margin';
	
	}

   return '<div class="column one-fifth column-last'.$margin.'">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
   
}
add_shortcode('one_fifth_last', 'si_one_fifth_last');



/*========================================================

	Accordion

========================================================*/

function si_accordion($atts, $content = null) {

	$content = str_replace('<br />', '', $content);

	return '<!-- Begin Toggle --><div class="si-accordion accordion shortcode">'.do_shortcode($content).'</div><!-- End Toggle -->';
	
}
add_shortcode( 'accordion', 'si_accordion' );

function si_accordion_panel($atts, $content = null) {

	extract(shortcode_atts(array(
		"title" => ''
	), $atts));

	$output = '<h3>'.$title.'</h3><div>'.do_shortcode($content).'</div>';
	
	return $output;
}
add_shortcode( 'panel', 'si_accordion_panel' );



/*========================================================

	Tabs

========================================================*/

add_shortcode( 'tabs', 'jquery_tab_group' );
function jquery_tab_group( $atts, $content ){
$GLOBALS['tab_count'] = 0;

do_shortcode( $content );

if( is_array( $GLOBALS['tabs'] ) ){
$int = '1';
foreach( $GLOBALS['tabs'] as $tab ){
$tabs[] = '

  <li><a href="#tabs-'.$int.'">'.$tab['title'].'</a></li>

';
$panes[] = '
<div id="tabs-'.$int.'">
'.$tab['content'].'
</div>
';
$int++;
}
$return = "\n".'
<div class="tabs">

<ul>'.implode( "\n", $tabs ).'</ul>

<div class="tabs-container">

'."\n".' '.implode( "\n", $panes ).'

</div>

</div>
'."\n";
}
return $return;
}

add_shortcode( 'tab', 'jquery_tab' );
function jquery_tab( $atts, $content ){
extract(shortcode_atts(array(
'title' => 'Tab %d'
), $atts));

$x = $GLOBALS['tab_count'];
$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'content' => $content );

$GLOBALS['tab_count']++;
}



/*========================================================

	Google Map

========================================================*/

function google_map($atts, $content = null) {

   extract(shortcode_atts(array(
      "width" => '640',
      "height" => '480',
      "src" => ''
   ), $atts));
   
   return '<div class="googlemap"><div class="googlemap_inner"><iframe width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$src.'&amp;output=embed"></iframe></div></div>';
   
}
add_shortcode("googlemap", "google_map");



?>