<?php get_header(); ?>

<?php while ( have_posts() ) : the_post();?>

	<article id="post_<?php the_ID(); ?>" <?php post_class("padL"); ?>>
		
		<?php
	
		$format = get_post_format();
		get_template_part( "formats/".$format );
		
		if($format == "")
		get_template_part( "formats/standard" );
		
		?>
		
	</article>
	
	<?php shorti_sticky_scroll(); ?>

<?php endwhile; ?> 

<?php get_footer(); ?>