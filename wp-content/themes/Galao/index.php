<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
								            
	<?php if (have_posts()) : $countposts = 0; while (have_posts()) : the_post(); $countposts++; ?>
	
		<!--BEGIN Article -->
		<article id="post-<?php the_ID(); ?>" <?php if ($countposts == 1) { post_class('first clearfix');} else{ post_class('clearfix'); } ?>>
									    						
			<div class="entry-content">
			
			<?php 
				$format = get_post_format();
				if( false === $format ) { $format = 'standard'; }
				get_template_part( 'includes/' . $format ); 
			?>
			
				<?php tj_post_content(); ?>

			</div>
			
			<?php if( $format == 'standard' || $format == 'gallery' || $format == 'image' || $format == 'video' || $format == 'audio') { ?>
			
			<div class="post-meta">
				<p><?php _e('Posted: ','framework'); ?><?php the_time(get_option('date_format')); ?> <?php _e('By: ','framework'); ?><?php the_author_posts_link(); ?></p>
				
				<p><?php _e('With: ','framework'); ?><a href="<?php comments_link(); ?>"><?php comments_number( '' . __('0 Comments', 'framework') . '', '' . __('1 Comment', 'framework') . '', '' . __('% Comments', 'framework') . '' ); ?></a></p>
				
			   <?php the_tags('<p class="tags">&#35;',' &#35;', '</p>' ); ?>
			   
			</div>
			
			<?php } ?>
		
		<!-- END Article -->
		</article>	
						
	<?php endwhile; ?>
				
		<?php if( function_exists( 'wp_pagenavi' ) ) : ?>
			<div class="pagination">
			
				<nav>
					
					<?php wp_pagenavi(); ?>
					
				</nav>	
				
			</div>
		<?php else : ?>
		
			<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				
				<div class="pagination-default">
					
					<nav>
						
						<div class="pagination-default-left"><?php next_posts_link( __( '&larr; Older posts', 'framework' ) ); ?></div>
						
						<div class="pagination-default-right"><?php previous_posts_link( __( 'Newer posts &rarr;', 'framework' ) ); ?></div>
				
					</nav>
					
				</div>
				
			<?php endif; ?>
			
		<?php endif; ?> 
				
	<?php else : ?>
	
	<!--BEGIN Article -->
	<article id="post-0" class="first clearfix">
	
		<div class="entry-content">							
														
			<h3><?php _e('404 - Not Found', 'framework') ?></h3>
		
			<p><?php _e("Sorry, but you are looking for something that isn't here.", "framework") ?></p>
			
		</div>
						
	<!--END Article-->
	</article>
				
	<?php endif; ?>
	
	</div>
	
	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>