<?php get_header(); ?>
	
<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
		
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<!-- BEGIN Article-->
		<article id="page-<?php the_ID(); ?>" <?php post_class('page-sidebar'); ?>>
				
				<div class="entry-content">
				
					<h1 class="entry-title"><?php the_title(); ?></h1>
				
					<?php the_content(); ?>
				
				</div>
				
			<!--END Article -->
		</article>
	<?php endwhile; endif; ?>
	
	</div>
	
	<?php get_sidebar(); ?>
		
</div>
			
<?php get_footer(); ?>