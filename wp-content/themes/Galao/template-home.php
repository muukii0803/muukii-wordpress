<?php/*Template Name: Page - Home*/?><?php get_header(); ?>

<div class="fullwidth" <?php tj_hero_banner(); ?>>
	
	<?php $tj_feature_image_link = of_get_option('tj_feature_image_link'); ?>
	
	<?php if ( $tj_feature_image_link ) : ?>
	
		<a href="<?php echo $tj_feature_image_link; ?>" target="_blank" class="clearfix tj-feature-image-link"></a>
	
	<?php endif; ?>
	
	<?php tj_welcome(); ?>
	
</div>

<?php 
if(of_get_option('show_projects') =='1' ){ 
	get_template_part( 'includes/home-recent-projects' ); 
} 
?>

<?php 
if(of_get_option('show_posts') =='1' ){ 
	get_template_part( 'includes/home-recent-posts' ); 
} 
?>

<?php get_footer(); ?>