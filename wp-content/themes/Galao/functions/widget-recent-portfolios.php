<?php

/*-----------------------------------------------------------------------------------

	Plugin Name: Recent Portfolio Widget
	Plugin URI: http://www.themejug.com
	Description: Display recent portfolio posts from the post type portfolio.
	Version: 1.0
	Author: TJ
	Author URI: http://www.themejug.com

-----------------------------------------------------------------------------------*/


// Add function to widgets_init that'll load our widget.
add_action( 'widgets_init', 'tj_recent_portfolios_widget' );


// Register widget.
function tj_recent_portfolios_widget() {
	register_widget( 'tj_recent_portfolios_widget' );
}

// Widget class.
class tj_recent_portfolios_widget extends WP_Widget {


/*-----------------------------------------------------------------------------------*/
/*	Widget Setup
/*-----------------------------------------------------------------------------------*/
	
	function tj_recent_portfolios_widget() {
	
		/* Widget settings. */
		$widget_ops = array( 
		    'classname' => 'tj_recent_portfolios_widget', 
		    'description' => __('Display recent portfolio posts from the post type portfolio.', 'framework') 
		);

		/* Widget control settings. */
		$control_ops = array( 
		    'width' => 300, 
		    'height' => 350, 
		    'id_base' => 'tj_recent_portfolios_widget' 
		);

		/* Create the widget. */
		$this->WP_Widget( 'tj_recent_portfolios_widget', __('ThemeJug - Recent Portfolios', 'framework'), $widget_ops, $control_ops );
	}


/*-----------------------------------------------------------------------------------*/
/*	Display Widget
/*-----------------------------------------------------------------------------------*/
	
	function widget( $args, $instance ) {
		extract( $args );
		
		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$number = ( isset($instance['number']) ) ? $instance['number'] : 0;

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title ) { echo $before_title . $title . $after_title; }
?>
                                 
	<?php 
	    $args = array(
            'posts_per_page' => $number,
            'ignore_sticky_posts' => 1,
            'post_type' => 'portfolio',
            'order' => 'DESC',
            'orderby' => 'date'
        );
        $port_query = new WP_Query( $args );
                       
        if( $port_query->have_posts() ) : $countposts = 0; while( $port_query->have_posts() ) : $port_query->the_post(); $countposts++; ?>
        
			<!-- BEGIN Article-->
			<article>
				<?php if( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
					<div class="blog-hero"><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('project-thumb'); ?></a></div>
				<?php } ?>
			<!-- END Article -->	                   
			</article>

        <?php endwhile; endif; ?>
        <?php wp_reset_postdata(); ?>

	<?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

/*-----------------------------------------------------------------------------------*/
/*	Update Widget
/*-----------------------------------------------------------------------------------*/
	
	function update( $new_instance, $old_instance ) {
		
		$instance = $old_instance;
		
		/* Strip tags to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = strip_tags( $new_instance['number'] );

		return $instance;
	}
	

/*-----------------------------------------------------------------------------------*/
/*	Widget Settings
/*-----------------------------------------------------------------------------------*/
	 
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array(
		'title' => 'Work',
		'number' => 6
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
        <!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'framework') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
        
		<!-- Number Input: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Amount to show:', 'framework') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" />
		</p>

	<?php
	}
}
?>