<?php

/*-----------------------------------------------------------------------------------

	Add Post Types

-----------------------------------------------------------------------------------*/


/* Create the Portfolio Custom Post Type ------------------------------------------*/
function tj_create_post_type_portfolio() 
{
	$labels = array(
		'name' => __( 'Portfolios','framework'),
		'singular_name' => __( 'Portfolio','framework' ),
		'add_new' => __('Add New','framework'),
		'add_new_item' => __('Add New Portfolio','framework'),
		'edit_item' => __('Edit Portfolio','framework'),
		'new_item' => __('New Portfolio','framework'),
		'view_item' => __('View Portfolio','framework'),
		'search_items' => __('Search Portfolio','framework'),
		'not_found' =>  __('No portfolio found','framework'),
		'not_found_in_trash' => __('No portfolio found in Trash','framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon' => get_bloginfo('template_directory') . '/img/portfolio_icon.png',
		// Uncomment the following line to change the slug; 
		// You must also save your permalink structure to prevent 404 errors
		//'rewrite' => array( 'slug' => 'portfolio' ), 
		'supports' => array('title','editor','thumbnail','custom-fields','page-attributes','excerpt')
	  ); 
	  
	  register_post_type(__( 'portfolio', 'framework' ),$args);
}

/* Create the Testimonial Custom Post Type ------------------------------------------*/

function tj_create_post_type_testimonial() 
{
	$labels = array(
		'name' => __( 'Testimonials','framework'),
		'singular_name' => __( 'Testimonial','framework' ),
		'add_new' => __('Add New','framework'),
		'add_new_item' => __('Add New Testimonial','framework'),
		'edit_item' => __('Edit Testimonial','framework'),
		'new_item' => __('New Testimonial','framework'),
		'view_item' => __('View Testimonials','framework'),
		'search_items' => __('Search Testimonials','framework'),
		'not_found' =>  __('No Testimonials found','framework'),
		'not_found_in_trash' => __('No Testimonials found in Trash','framework'), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		// Uncomment the following line to change the slug; 
		// You must also save your permalink structure to prevent 404 errors
		//'rewrite' => array( 'slug' => 'testimonials-slug' ), 
		'supports' => array('title','editor', 'custom-fields')
	  ); 
	  
	  register_post_type(__( 'testimonial', 'framework' ),$args);
}

function tj_build_taxonomies(){
/* Create the Portfolio Type Taxonomy --------------------------------------------*/
    $labels = array(
        'name' => __( 'Portfolio Type', 'framework' ),
        'singular_name' => __( 'Portfolio Type', 'framework' ),
        'search_items' =>  __( 'Search Portfolio Types', 'framework' ),
        'popular_items' => __( 'Popular Portfolio Types', 'framework' ),
        'all_items' => __( 'All Portfolio Types', 'framework' ),
        'parent_item' => __( 'Parent Portfolio Type', 'framework' ),
        'parent_item_colon' => __( 'Parent Portfolio Type:', 'framework' ),
        'edit_item' => __( 'Edit Portfolio Type', 'framework' ), 
        'update_item' => __( 'Update Portfolio Type', 'framework' ),
        'add_new_item' => __( 'Add New Portfolio Type', 'framework' ),
        'new_item_name' => __( 'New Portfolio Type Name', 'framework' ),
        'separate_items_with_commas' => __( 'Separate portfolio types with commas', 'framework' ),
        'add_or_remove_items' => __( 'Add or remove portfolio types', 'framework' ),
        'choose_from_most_used' => __( 'Choose from the most used portfolio types', 'framework' ),
        'menu_name' => __( 'Portfolio Types', 'framework' )
    );
    
	register_taxonomy(
	    'portfolio-type', 
	    array( __( 'portfolio', 'framework' )), 
	    array(
	        'hierarchical' => true, 
	        'labels' => $labels,
	        'show_ui' => true,
	        'query_var' => true,
	        'rewrite' => array('slug' => 'portfolio-type', 'hierarchical' => true)
	    )
	);
	
}

/* Enable Sorting of the Portfolio ------------------------------------------*/
function tj_create_portfolio_sort_page() {
    $tj_sort_page = add_submenu_page('edit.php?post_type=portfolio', 'Sort Portfolios', __('Sort Portfolios', 'framework'), 'edit_posts', basename(__FILE__), 'tj_portfolio_sort');
    
    add_action('admin_print_styles-' . $tj_sort_page, 'tj_print_sort_styles');
    add_action('admin_print_scripts-' . $tj_sort_page, 'tj_print_sort_scripts');
}

function tj_portfolio_sort() {
    $portfolios = new WP_Query('post_type=portfolio&posts_per_page=-1&orderby=menu_order&order=ASC');
?>
    <div class="wrap">
        <div id="icon-tools" class="icon32"><br /></div>
        <h2><?php _e('Sort Portfolios', 'framework'); ?></h2>
        <p><?php _e('Click, drag and re-order. Portfolio item at the top will appear first.', 'framework'); ?></p>

        <ul id="portfolio_list">
            <?php while( $portfolios->have_posts() ) : $portfolios->the_post(); ?>
                <?php if( get_post_status() == 'publish' ) { ?>
                	<?php $tags = get_the_term_list( get_the_ID(), 'portfolio-type', '', ', ', '' ); ?>
                    <li id="<?php the_id(); ?>" class="menu-item">
                        <dl class="menu-item-bar">
                            <dt class="menu-item-handle">
                                <span class="menu-item-title"><strong><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong></span>
                                <?php if( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
                                	<span class="menu-item-thumbnail"><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', array (32,32) ); ?></a></span>
                                <?php } ?>
                               <?php 
                               if ( $tags ) { 
                               		echo '<span  class="menu-item-tags">'. $tags . '</span>'; 
                               } else {
                               _e('No Portfolio Tags','framework');
                               }
                               ?>
                               <span class="menu-item-date"><small><?php the_time(get_option('date_format')); ?></small></span>
                            </dt>
                        </dl>
                        <ul class="menu-item-transport"></ul>
                    </li>
                <?php } ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </ul>
    </div>
<?php }

function tj_save_portfolio_sorted_order() {
    global $wpdb;
    
    $order = explode(',', $_POST['order']);
    $counter = 0;
    
    foreach($order as $portfolio_id) {
        $wpdb->update($wpdb->posts, array('menu_order' => $counter), array('ID' => $portfolio_id));
        $counter++;
    }
    die(1);
}

function tj_print_sort_scripts() {
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('tj_portfolio_sort', get_template_directory_uri() . '/functions/js/tj_portfolio_sort.js');
}

function tj_print_sort_styles() {
	wp_register_style( 'portfolio-sort', get_template_directory_uri() . '/css/portfolio-sort.css', array(), '', 'all' );
    wp_enqueue_style('portfolio-sort');
}


/* Add Custom Columns ------------------------------------------------------*/
function tj_portfolio_edit_columns($columns){  

        $columns = array(  
            "cb" => "<input type=\"checkbox\" />",  
            "title" => __( 'Portfolio Item Title' , 'framework'),
            "type" => __( 'Portfolio Item Tags', 'framework' )
        );  
  
        return $columns;  
}  
  
function tj_portfolio_custom_columns($column){  
        global $post;  
        switch ($column)  
        {    
            case __( 'type', 'framework' ):  
                echo get_the_term_list($post->ID, __( 'portfolio-type', 'framework' ), '', ', ','');  
                break;
        }  
} 

/* Call our custom functions ---------------------------------------------*/
add_action( 'init', 'tj_create_post_type_portfolio' );
add_action( 'init', 'tj_create_post_type_testimonial' );
add_action( 'init', 'tj_build_taxonomies', 0 );

add_action('admin_menu', 'tj_create_portfolio_sort_page');
add_action('wp_ajax_portfolio_sort', 'tj_save_portfolio_sorted_order');

add_filter("manage_edit-portfolio_columns", "tj_portfolio_edit_columns");  
add_action("manage_posts_custom_column",  "tj_portfolio_custom_columns");