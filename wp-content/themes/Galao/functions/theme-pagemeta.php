<?php

/*-----------------------------------------------------------------------------------

	Add post meta boxes to Page items

-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/*	Define Metabox Fields
/*-----------------------------------------------------------------------------------*/

$prefix = 'tj_';

$meta_box_pagefeature = array(
	'id' => 'tj-meta-box-featured',
	'title' =>  __('Featured On Homepage', 'framework'),
	'page' => 'page',
	'context' => 'side',
	'priority' => '',
	'fields' => array(
		array( "name" => __('Check To Feature Page','framework'),
				"desc" => __('Check to feature page on the homepage.','framework'),
				"id" => $prefix."feature",
				"type" => "checkbox"
				
			),
	)	
);

$meta_box_pagefeature_url = array(
	'id' => 'tj-meta-box-featured-url',
	'title' =>  __('Homepage Thumbnail URL', 'framework'),
	'page' => 'page',
	'context' => 'side',
	'priority' => '',
	'fields' => array(
		array( "name" => __('URL: ','framework'),
				"desc" => __('','framework'),
				"id" => $prefix."feature-url",
				"type" => "text",
				'std' => 'http://'
			)
	)	
);

add_action('admin_menu', 'tj_add_box_page');


/*-----------------------------------------------------------------------------------*/
/*	Add metabox to edit page
/*-----------------------------------------------------------------------------------*/
 
function tj_add_box_page() {
	global $meta_box_pagefeature, $meta_box_pagefeature_url;
 
	add_meta_box($meta_box_pagefeature['id'], $meta_box_pagefeature['title'], 'tj_show_box_feature', $meta_box_pagefeature['page'], $meta_box_pagefeature['context'], $meta_box_pagefeature['priority']);
	
	add_meta_box($meta_box_pagefeature_url['id'], $meta_box_pagefeature_url['title'], 'tj_show_box_feature_url', $meta_box_pagefeature_url['page'], $meta_box_pagefeature_url['context'], $meta_box_pagefeature_url['priority']);

}

/*-----------------------------------------------------------------------------------*/
/*	Show populated fields in meta box
/*-----------------------------------------------------------------------------------*/

function tj_show_box_feature() {
	global $meta_box_pagefeature, $post;

	// Use nonce for verification
	echo '<input type="hidden" name="tj_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
 
	echo '<table class="form-table">';
 
	foreach ($meta_box_pagefeature['fields'] as $field) {
		// get current post meta data
		$meta = get_post_meta($post->ID, $field['id'], true);
		switch ($field['type']) {
 
			//If checkbox		
			case 'checkbox':
			
			echo '<tr>',
				'<th style="width:100%"><label for="', $field['id'], '"><strong>', $field['name'], '</strong><span style="line-height:18px; display:block; color:#999; margin:5px 0 0 0;">'. $field['desc'].'</span></label></th>',
				'<td>';
			echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
			
			
			break;

		}

	}
 
	echo '</table>';
}

function tj_show_box_feature_url() {
	global $meta_box_pagefeature_url, $post;
 	
	// Use nonce for verification
	echo '<input type="hidden" name="tj_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
 
	echo '<table class="form-table">';
 
	foreach ($meta_box_pagefeature_url['fields'] as $field) {
		// get current post meta data
		$meta = get_post_meta($post->ID, $field['id'], true);
		switch ($field['type']) {
 
			
			//If Text		
			case 'text':
			
			echo '<tr style="border-top:1px solid #eeeeee;">',
				'<th style="width:25%"><label for="', $field['id'], '"><strong>', $field['name'], '</strong><span style=" display:block; color:#999; margin:5px 0 0 0; line-height: 18px;">'. $field['desc'].'</span></label></th>',
				'<td>';
			echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : stripslashes(htmlspecialchars(( $field['std']), ENT_QUOTES)), '" size="30" style="width:75%; margin-right: 20px; float:left;" />';
			
			break;
 
			//If Button	
			case 'button':
				echo '<input style="float: left;" type="button" class="button" name="', $field['id'], '" id="', $field['id'], '"value="', $meta ? $meta : $field['std'], '" />';
				echo 	'</td>',
			'</tr>';
			
			break;
			
			//If Select	
			case 'select':
			
				echo '<tr>',
				'<th style="width:25%"><label for="', $field['id'], '"><strong>', $field['name'], '</strong><span style=" display:block; color:#999; margin:5px 0 0 0; line-height: 18px;">'. $field['desc'].'</span></label></th>',
				'<td>';
			
				echo'<select id="' . $field['id'] . '" name="'.$field['id'].'">';
			
				foreach ($field['options'] as $option) {
					
					echo'<option';
					if ($meta == $option ) { 
						echo ' selected="selected"'; 
					}
					echo'>'. $option .'</option>';
				
				} 
				
				echo'</select>';
			
			break;
			
            case 'color':

                echo '<tr>',
    			'<th style="width:25%"><label for="', $field['id'], '"><strong>', $field['name'], '</strong><span style=" display:block; color:#999; margin:5px 0 0 0; line-height: 18px;">'. $field['desc'].'</span></label></th>',
    			'<td>';

                echo '<div id="' . $field['id'] . '_picker" class="colorSelector"><div></div></div>';
    			echo '<input style="width:75px; margin-left: 5px;" class="tj-color" name="'. $field['id'] .'" id="'. $field['id'] .'" type="text" value="'. $meta .'" />';
?>   			
    			<script type="text/javascript" language="javascript">
            		jQuery(document).ready(function(){
            			//Color Picker
    				    jQuery('#<?php echo $field['id']; ?>_picker').children('div').css('backgroundColor', '<?php echo $meta; ?>');    
            			jQuery('#<?php echo $field['id']; ?>_picker').ColorPicker({
            				color: '<?php echo $meta; ?>',
            				onShow: function (colpkr) {
            					jQuery(colpkr).fadeIn(500);
            					return false;
            				},
            				onHide: function (colpkr) {
            					jQuery(colpkr).fadeOut(500);
            					return false;
            				},
            				onChange: function (hsb, hex, rgb) {
            					//jQuery(this).css('border','1px solid red');
            					jQuery('#<?php echo $field['id']; ?>_picker').children('div').css('backgroundColor', '#' + hex);
            					jQuery('#<?php echo $field['id']; ?>_picker').next('input').attr('value','#' + hex);
        					}
    				    });
                    });
        		</script>
<?php       break;             
			
		}

	}
 
	echo '</table>';
}

add_action('save_post', 'tj_save_data_page');


/*-----------------------------------------------------------------------------------*/
/*	Save data when post is edited
/*-----------------------------------------------------------------------------------*/
 
function tj_save_data_page($post_id) {
	global $meta_box_pagefeature, $meta_box_pagefeature_url;
 
	// verify nonce
	if (!isset($_POST['tj_meta_box_nonce']) || !wp_verify_nonce($_POST['tj_meta_box_nonce'], basename(__FILE__))) {
		return $post_id;
	}
 
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $post_id;
	}
 
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		}
	} elseif (!current_user_can('edit_post', $post_id)) {
		return $post_id;
	}
 
	foreach ($meta_box_pagefeature['fields'] as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		
		if(!isset($_POST[$field['id']])) {
			$new = ""; 
		}else {
			$new = $_POST[$field['id']];
		}
 
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], stripslashes(htmlspecialchars($new)));
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	}
	
	foreach ($meta_box_pagefeature_url['fields'] as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		
		if(!isset($_POST[$field['id']])) {
			$new = ""; 
		}else {
			$new = $_POST[$field['id']];
		}

		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], stripslashes(htmlspecialchars($new)));
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	}
		
}