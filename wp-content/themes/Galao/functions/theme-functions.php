<?php

/*-----------------------------------------------------------------------------------*/
/* Output Custom CSS from theme options
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_head_css' ) ) {
    function tj_head_css() {
        
        $output = '';		
        
        /* Custom Background Color */
        
        $bg_color = of_get_option('bg_color');
        
        if ( !empty($bg_color) ) {
        	$output .= "body { background: " . $bg_color . "; }\n\n";
        }
        
        /* Custom Background Image */
        $bg_image = of_get_option('bg_image');
        /* Get Custom Background Position */
        $bg_position = of_get_option('bg_image_position');
        /* Custom Background Repeat */
        $bg_repeat = of_get_option('bg_image_repeat');
        
       	if ( !empty($bg_image) ) {

			if ( 'fullscreen' != $bg_position ) {
		        
		        if ( !empty($bg_image) ) {
		        	$output .= "body { background-image:url('" . $bg_image . "')!important; }\n.home-projects-wrapper,\n.home-projects-full-wrapper { background: transparent; }\n";
		        }
		        
		        if ( !empty($bg_position) ) {
		        	$output .= "body { background-position: " . $bg_position . "; }\n\n";
		        }
		        
		        if ( !empty($bg_repeat) ) {
		        	$output .= "body { background-repeat: " . $bg_repeat . "; }\n\n";
		        }
	        
			} elseif ( 'fullscreen' == $bg_position ) {
				$output .="body { background: url(" . $bg_image . ") no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; } \n\n.home-projects-wrapper,\n.home-projects-full-wrapper { background: transparent; }\n\n";
			}
		
		}
		
		/* Custom Heading Color */
		$heading_color = of_get_option('heading_color');
		
		if ( !empty($heading_color) ) {
			$output .= "h1,\nh2,\nh3,\nh4,\nh5,\nh6 { color: " . $heading_color . "!important;} \n\n";
		}
		
		/* Custom Text Color */
		$text_color = of_get_option('text_color');
		
		if ( !empty($text_color) ) {
			$output .= "body, p { color: " . $text_color . "; } \n\n";
		}
				
		/* Custom Link Color */
		$link_color = of_get_option('link_color');
		
		if ( !empty($link_color) ) {
			$output .= ".pagination-default,\n.portfolio-nav,\n.read-more a,\n.twitter-follow a,\n.commentlist a,\n.portfolio-entry a,\n.tweetings a,\n.tj_tweet_widget p.twitter-follow,\n.entry-content a,\n.entry_content p a,\n#sidebar .widget_text a,\n#sidebar .textwidget a:active,\n#sidebar .textwidget a:visited,\n.tj_recent_portfolios_widget .entry-title a,\n.testimonial a,\n.home-caption a,\n.author-description a,\n.tj-posts-widget a,\n#sort-by ul li a { color: " . $link_color . "!important;} \n\n";
			
			$output .= ".pagination-default-left a,\n.pagination-default-right a,\n.tj_tweet_widget .twitter-follow a,\n#respond input[type=\"submit\"],\n#contact input[type=\"submit\"] { background-color: " . $link_color . "; }\n\n";
		}
		
		/* Custom Link Hover Color */
		$link_hover_color = of_get_option('link_hover_color');
		
		if ( !empty($link_hover_color) ) {
			$output .= ".entry-title a:hover,\n.post-meta a:hover,\n.pagination-default-left a:hover,\n.pagination-default-right a:hover,\n.pagination-portfolio p a:hover,\n.read-more a:hover,\n.footernav a:hover,\n.commentlist a:hover,\n.single-portfolio a:hover,\n.entry-content a:hover,\n.entry_content p a:hover,\n.widget_text a:hover,\n.tj_tweet_widget .status a:hover,\n.tj_recent_portfolios_widget a:hover,\nnav ul li a:hover,\nnav ul li.sfHover ul li.current-menu-item a,\nnav ul li.current-menu-parent a,\nheader nav ul li.current-menu-item a,\nnav ul ul li a:hover,\nnav ul ul li.sfHover a,\nnav ul ul li.current-cat a,\nnav ul li.current_page_item a,\nnav ul li.current-menu-item a,\nnav ul li.sfHover ul a:hover,\n.widget ul li a:hover,\n#sidebar .widget_text a:hover,\n.twitter-meta:hover,\n.home-message a:hover,\n.testimonial a:hover,\n.home-caption a:hover,\n#footer a:hover,\n.author-description a:hover,\n.tj-posts-widget a:hover,\n#sort-by ul li a:hover,\n#sort-by .active { color: " . $link_hover_color . "!important; }\n\n";
			
			$output .= ".pagination-default-left a:hover,\n.pagination-default-right a:hover,\n.flex-direction-nav li a:hover,\n.tj_tweet_widget .twitter-follow a:hover,\n.tj_flickr_widget .flickrwidget .flickr_badge_image:hover,\n.tj_recent_portfolios_widget article:hover,\n#respond input[type=\"submit\"]:hover,\n#contact input[type=\"submit\"]:hover,\n::selection { background-color: " . $link_hover_color . "; }\n\n";
		}
		
		/* Output the above */
		if ($output <> '') {
			return stripslashes($output);
		}
	
    }

}

/*-----------------------------------------------------------------------------------*/
/* Combine Custom & Default CSS
/*-----------------------------------------------------------------------------------*/

function tj_admin_css($content) {
    $tj_css = of_get_option('custom_css')."\n\n";
    $tj_css .= tj_head_css()."\n\n";
    if( $tj_css != '' ){
    	$content .= '/* CSS Output From Theme Options */' . "\n";
        $content .= stripslashes($tj_css);
        $content .= "\n\n";
    }
    return $content;
    
}
add_filter( 'tj_add_admin_css', 'tj_admin_css' );

/*-----------------------------------------------------------------------------------*/
/* Build Custom CSS File
/*-----------------------------------------------------------------------------------*/
 
function tj_add_admin_css() {
    $output = '';
    if( apply_filters('tj_add_admin_css', $output) ) {
    	$permalink_structure = get_option('permalink_structure');
    	$url = home_url() .'/tj-admin-options.css?'. time();
    	if(!$permalink_structure) $url = home_url() .'/?page_id=tj-admin-options.css';
        echo '<link rel="stylesheet" href="'. $url .'" type="text/css" media="screen" />' . "\n";
    }
}
add_action( 'wp_head', 'tj_add_admin_css', 12 );

/*-----------------------------------------------------------------------------------*/
/* Link To Custom CSS File
/*-----------------------------------------------------------------------------------*/

function tj_create_admin_css() {
	$permalink_structure = get_option('permalink_structure');
	$show_css = false;

	if($permalink_structure){
		if( !isset($_SERVER['REQUEST_URI']) ){
		    $_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);
		    if(isset($_SERVER['QUERY_STRING'])){ $_SERVER['REQUEST_URI'].='?'.$_SERVER['QUERY_STRING']; }
		}
		$url = (isset($GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'])) ? $GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'] : $_SERVER["REQUEST_URI"];
		if(preg_replace('/\\?.*/', '', basename($url)) == 'tj-admin-options.css') $show_css = true;
	} else {
		if(isset($_GET['page_id']) && $_GET['page_id'] == 'tj-admin-options.css') $show_css = true;
	}

	if($show_css){
	    $output = '';
		header('Content-Type: text/css');
		echo apply_filters('tj_add_admin_css', $output);
		exit;
	}
}
add_action( 'init', 'tj_create_admin_css' );

/*-----------------------------------------------------------------------------------*/
/* Homepage Message
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_welcome' ) ) {
	function tj_welcome() {
		if ( of_get_option('hi_message') != '' && is_page_template('template-home.php') ){
			$output = of_get_option('hi_message');
			$content = apply_filters('the_content', $output);
			echo '<div class="home-message">';
			echo $content;
			echo '</div>';
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/* Homepage Full Width Banner
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_hero_banner' ) ) {
	function tj_hero_banner() {
		if ( of_get_option('tj_feature_image') != '' && is_page_template('template-home.php') ) {
			$tj_feature_image_bg = of_get_option('tj_feature_image_bg');
			$tj_feature_image = of_get_option('tj_feature_image');
			$tj_feature_image_height = of_get_option('tj_feature_image_height');
			
			if ( $tj_feature_image != '') { 
			
				if ( $tj_feature_image_height ) {
				
					echo 'style="background: url(' . $tj_feature_image . ') no-repeat center center; min-height: ' . $tj_feature_image_height . 'px; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: inline-block;"'; 
				
				} else {
				
					echo 'style="background: url(' . $tj_feature_image . ') no-repeat center center; min-height: 550px; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; display: inline-block;"'; 
					
				}
			
			}

		}
	}
}

/*-----------------------------------------------------------------------------------*/
/* Portfolio Message
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_portfolio_welcome' ) ) {
	function tj_portfolio_welcome() {
		if ( of_get_option('project_title') != '' && is_page_template('template-portfolio-paginated.php') || is_page_template('template-portfolio-filtered.php') ){
			$output = of_get_option('show_projects_caption');
			$content = apply_filters('the_content', $output);
			echo '<div class="tj_portfolio_welcome">';
			echo '<h2>' . of_get_option('project_title') . '</h2>';
			echo '<div class="seperator clearfix"></div>';
			echo $content;
			echo '</div>';
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/* Blog Posts - display the_excerpt OR the_content
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_post_content' ) ) {
	function tj_post_content() {
		
		$use_excerpt = of_get_option('use_excerpt');
		if ($use_excerpt == 'excerpt') {
			the_excerpt();
		} elseif ($use_excerpt == 'content') {
			the_content(__('Continue reading','framework'));
		} else {
			the_excerpt();
		}
		
	}
}

/*-----------------------------------------------------------------------------------*/
/* Remove the_content <!-- More --> Anchor
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_remove_more_anchor' ) ) {
	function tj_remove_more_anchor($link) {
		$offset = strpos($link, '#more-');
		if ($offset) { $end = strpos($link, '"',$offset); }
		if ($end) { $link = substr_replace($link, '', $offset, $end-$offset); }
		return $link;
	}
	add_filter('the_content_more_link', 'tj_remove_more_anchor');
}
    
/*-----------------------------------------------------------------------------------*/
/* Add iPad / iPhone Icon
/*-----------------------------------------------------------------------------------*/

function tj_custom_ico() {
	if ( of_get_option('iphone_ico_uploader') != '') {
	echo '<link rel="apple-touch-icon-precomposed" href="' . of_get_option('iphone_ico_uploader') . '" />'."\n";
	}
	else { ?>
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri() ?>/img/apple-touch-icon.png" />
	<?php }
}

add_action('wp_head', 'tj_custom_ico');

/*-----------------------------------------------------------------------------------*/
/* Add Favicon
/*-----------------------------------------------------------------------------------*/

function tj_custom_favicon() {
	if ( of_get_option('favicon_uploader') != '') {
		echo '<link rel="shortcut icon" href="'. of_get_option('favicon_uploader') .'"/>'."\n";
	}
	else { ?>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/favicon.png" />
	<?php }
}

add_action('wp_head', 'tj_custom_favicon');

/*-----------------------------------------------------------------------------------*/
/*	Get related posts by taxonomy
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_get_posts_related_by_taxonomy' ) ) {
    function tj_get_posts_related_by_taxonomy($post_id, $taxonomy, $args=array()) {
        $query = new WP_Query();
        $terms = wp_get_object_terms($post_id, $taxonomy);
        if (count($terms)) {
        // Assumes only one term for per post in this taxonomy
        $post_ids = get_objects_in_term($terms[0]->term_id,$taxonomy);
        $post = get_post($post_id);
        $args = wp_parse_args($args,array(
            'post_type' => $post->post_type, // The assumes the post types match
            'post__not_in' => array($post_id),
            'taxonomy' => $taxonomy,
            'term' => $terms[0]->slug,
            'orderby' => 'rand',
            'posts_per_page' => get_option('tj_portfolio_related_number')
        ));
        $query = new WP_Query($args);
        }
        return $query;
    }
}


/*-----------------------------------------------------------------------------------*/
/* Output image
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_image' ) ) {
    function tj_image($postid, $imagesize) {
		global $post;
		
		// Get featured thumb ID, 
		$thumb_ID = get_post_thumbnail_id( $post->ID );
		
		if ( 'portfolio' === get_post_type( $postid ) ) {
			$imgClass = "portfolio";
		}else {
			$imgClass = "blog";
		}
		
		// Exclude Or Include The Featured Image In Content
		if (of_get_option('exclude_featured_image') == '1') {
		
			$args = array(
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_type' => 'attachment',
				'post_parent' => $postid,
				'post_mime_type' => 'image',
				'post_status' => null,
				'numberposts' => -1,
				'exclude' => $thumb_ID
			);
			$attachments = get_posts($args);
		
		}else {
		
			$args = array(
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_type' => 'attachment',
				'post_parent' => $postid,
				'post_mime_type' => 'image',
				'post_status' => null,
				'numberposts' => -1
			);
			$attachments = get_posts($args);
		
		}

		if( !empty($attachments) ) {
		    echo '<div class="' . $imgClass . '">';
		    $i = 0;
		    foreach( $attachments as $attachment ) {
		        $src = wp_get_attachment_image_src( $attachment->ID, $imagesize );
		        $alt = ( !empty($attachment->post_content) ) ? $attachment->post_content : $attachment->post_title;
		        echo "<img height='$src[2]' width='$src[1]' src='$src[0]' alt='$alt' />";
		        $i++;
		    }
		    echo '</div>';
		}
    }
}

/*-----------------------------------------------------------------------------------*/
/* Output gallery slideshow
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_gallery' ) ) {
    function tj_gallery($postid, $imagesize) { ?>
		<script type="text/javascript" charset="utf-8">
		  $(window).load(function() {
			$('.flexslider').flexslider({
			 animation: 'fade',
			 slideshow: true,
			 directionNav: true,
			 controlNav: false,
			 keyboardNav: true,
			 mousewheel: false,
			 touch: true,
			 smoothHeight: true
			});
		  });
		</script>
    <?php 
    	global $post;
    	// get featured thumb ID, 
    	$thumb_ID = get_post_thumbnail_id( $post->ID );
        
		// Exclude Or Include The Featured Image In Content
		if (of_get_option('exclude_featured_image') == '1') {
		
			$args = array(
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_type' => 'attachment',
				'post_parent' => $postid,
				'post_mime_type' => 'image',
				'post_status' => null,
				'numberposts' => -1,
				'exclude' => $thumb_ID
			);
			$attachments = get_posts($args);
		
		}else {
		
			$args = array(
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_type' => 'attachment',
				'post_parent' => $postid,
				'post_mime_type' => 'image',
				'post_status' => null,
				'numberposts' => -1
			);
			$attachments = get_posts($args);
		
		}
        
        if( !empty($attachments) ) {
            echo '<div class="flexslider">';
            	echo '<ul class="slides">';
            $i = 0;
            foreach( $attachments as $attachment ) {
                $src = wp_get_attachment_image_src( $attachment->ID, $imagesize );
                $alt = ( !empty($attachment->post_content) ) ? $attachment->post_content : $attachment->post_title;
                echo "<li><img height='$src[2]' width='$src[1]' src='$src[0]' alt='$alt' /></li>";
                $i++;
            }
            	echo '</ul>';
            echo '</div>';
        }
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Output Audio
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_audio' ) ) {
    function tj_audio($postid, $width, $type) {
    	$mp3 = get_post_meta($postid, 'tj_audio_mp3', true);
    	$ogg = get_post_meta($postid, 'tj_audio_ogg', true);
    	$height = '';
    	$width = '';
    	global $post;
	?>
    		<script type="text/javascript">
		
    			jQuery(document).ready(function(){
	
    				if(jQuery().jPlayer) {
    					jQuery("#jquery_jplayer_<?php echo $postid; ?>").jPlayer({
    						ready: function () {
    							jQuery(this).jPlayer("setMedia", {
    							<?php if($mp3 != '') : ?>
    								mp3: "<?php echo $mp3; ?>",
    							<?php endif; ?>
    							<?php if($ogg != '') : ?>
    								oga: "<?php echo $ogg; ?>",
    							<?php endif; ?>
								 <?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { 
									//portfolio or blog post hero image
									if ($type == "portfolio") {
										$width = $width;
									  	$poster = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog-thumb', false, '' );
									  	$height = $poster[2];
									 	$poster = $poster[0];
									}else {
									  	$width = "100%";
									  	$poster = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog-full', false, '' );
									  	$poster = $poster[0];
									}
								  ?>
								    poster: "<?php echo $poster; ?>",
								<?php } ?>    							
    							end: ""
    							});
    						},
            				size: {
            				    width: "<?php echo $width; ?>",
            				    height: "<?php echo $height; ?>"
            				},
    						swfPath: "<?php echo get_template_directory_uri(); ?>/js",
    						cssSelectorAncestor: "#jp_interface_<?php echo $postid; ?>",
    						supplied: "<?php if($ogg != '') : ?>oga, <?php endif; ?><?php if($mp3 != '') : ?>mp3, <?php endif; ?>all"
    					});
					
    				}
    			});
    		</script>
		
    	    <div id="jquery_jplayer_<?php echo $postid; ?>" class="jp-jplayer jp-jplayer-audio glossy-jp"></div>

            <div class="jp-audio-container">
                <div class="jp-audio">
                    <div class="jp-type-single">
                        <div id="jp_interface_<?php echo $postid; ?>" class="jp-interface">
                            <ul class="jp-controls">
                            	<li><div class="seperator-first"></div></li>
                                <li><div class="seperator-second"></div></li>
                                <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                                <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                                <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                                <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                            </ul>
                            <div class="jp-progress-container">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-volume-bar-container">
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    	<?php 
    }
}


/*-----------------------------------------------------------------------------------*/
/* Output video
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tj_video' ) ) {
    function tj_video($postid, $width, $type) {
    	$m4v = get_post_meta($postid, 'tj_video_m4v', true);
    	$ogv = get_post_meta($postid, 'tj_video_ogv', true);
    	$height = '';
    	$width = '';
    	global $post;
    ?>
    <script type="text/javascript">
    	jQuery(document).ready(function(){
		
    		if(jQuery().jPlayer) {
    			jQuery("#jquery_jplayer_<?php echo $postid; ?>").jPlayer({
    				ready: function () {
    					jQuery(this).jPlayer("setMedia", {
    						<?php if($m4v != '') : ?>
    						m4v: "<?php echo $m4v; ?>",
    						<?php endif; ?>
    						<?php if($ogv != '') : ?>
    						ogv: "<?php echo $ogv; ?>",
    						<?php endif; ?> 
    						  						
							 <?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { 
							 	//portfolio or blog post feature image
							 	if ($type === "portfolio") {
							 		$width = "100%";
							    	$poster = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'project-full', false, '' );
							    	//$height = $poster[2];
							    	$poster = $poster[0];
							    	
							    }else {
							    	$width = "100%";
									$poster = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog-full', false, '' );
									$poster = $poster[0];
							    }
							  	?>
							    poster: "<?php echo $poster; ?>",
							    end: ""
							<?php } ?>
    						
    					});
    				},
    				size: {
    				    width: "<?php echo $width; ?>",
    				    height: "<?php echo $height; ?>"
    				},
    				swfPath: "<?php echo get_template_directory_uri(); ?>/js",
    				cssSelectorAncestor: "#jp_interface_<?php echo $postid; ?>",
    				supplied: "<?php if($m4v != '') : ?>m4v, <?php endif; ?><?php if($ogv != '') : ?>ogv, <?php endif; ?> all"
    			});
    		}
    	});
    </script>

    <div id="jquery_jplayer_<?php echo $postid; ?>" class="jp-jplayer jp-jplayer-video glossy-jp"></div>

    <div class="jp-video-container">
        <div class="jp-video">
            <div class="jp-type-single">
                <div id="jp_interface_<?php echo $postid; ?>" class="jp-interface">
                    <ul class="jp-controls">
                    	<li><div class="seperator-first"></div></li>
                        <li><div class="seperator-second"></div></li>
                        <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                        <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                        <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                        <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                    </ul>
                    <div class="jp-progress-container">
                        <div class="jp-progress">
                            <div class="jp-seek-bar">
                                <div class="jp-play-bar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="jp-volume-bar-container">
                        <div class="jp-volume-bar">
                            <div class="jp-volume-bar-value"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php }
}

/*-----------------------------------------------------------------------------------*/
/* Custom Walker for wp_list_categories in template-portfolio.php
/*-----------------------------------------------------------------------------------*/
/*
class Portfolio_Walker extends Walker_Category {
    function start_el(&$output, $category, $depth, $args) {
            extract($args);

            $cat_name = esc_attr( $category->name );
            $cat_name = apply_filters( 'list_cats', $cat_name, $category );
            $link = '<a href="' . esc_attr( get_term_link($category) ) . '" ';
            $link .= 'data-filter="' . urldecode($category->slug) . '" ';
           
            if ('dropdown' == $args['style'] ) {
            	$option = '' . urldecode($category->slug) . '';
            	$option_name = $cat_name;
            }
            
            if ( $use_desc_for_title == 0 || empty($category->description) )
                    $link .= 'title="' . esc_attr( sprintf(__( 'View all posts filed under %s', 'framework' ), $cat_name) ) . '"';
            else
                    $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
            $link .= '>';
            $link .= $cat_name . '</a>';

            if ( !empty($feed_image) || !empty($feed) ) {
                    $link .= ' ';

                    if ( empty($feed_image) )
                            $link .= '(';

                    $link .= '<a href="' . get_term_feed_link( $category->term_id, $category->taxonomy, $feed_type ) . '"';

                    if ( empty($feed) ) {
                            $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s', 'framework' ), $cat_name ) . '"';
                    } else {
                            $title = ' title="' . $feed . '"';
                            $alt = ' alt="' . $feed . '"';
                            $name = $feed;
                            $link .= $title;
                    }

                    $link .= '>';

                    if ( empty($feed_image) )
                            $link .= $name;
                    else
                            $link .= "<img src='$feed_image'$alt$title" . ' />';

                    $link .= '</a>';

                    if ( empty($feed_image) )
                            $link .= ')';
            }

            if ( !empty($show_count) )
                    $link .= ' (' . intval($category->count) . ')';

            if ( !empty($show_date) )
                    $link .= ' ' . gmdate('Y-m-d', $category->last_update_timestamp);

            if ( 'list' == $args['style'] ) {
                    $output .= "\t<li";
                    $class = 'cat-item cat-item-' . $category->term_id;
                    if ( !empty($current_category) ) {
                            $_current_category = get_term( $current_category, $category->taxonomy );
                            if ( $category->term_id == $current_category )
                                    $class .=  ' current-cat';
                            elseif ( $category->term_id == $_current_category->parent )
                                    $class .=  ' current-cat-parent';
                    }
                    $output .=  ' class="' . $class . '"';
                    $output .= ">$link\n";
            } elseif ('dropdown' == $args['style'] ) {
            	$output .= "<option value='." . $option . "'";
            	$output .= ">$option_name</option>";
            } 
            else {
                    $output .= "\t$link<br />\n";
            }
    }
}
*/
	
/*-----------------------------------------------------------------------------------*/
/*	HTML5 Comments
/*-----------------------------------------------------------------------------------*/
	
if ( ! function_exists( 'tj_html5comments' ) ) :

function tj_html5comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	
	<!-- BEGIN Comment Article -->
	<article <?php comment_class('clearfix'); ?> id="<?php comment_ID(); ?>" >
			
		<?php echo get_avatar( $comment, 60 ); ?>
	
		<!-- BEGIN .entry_info -->
		<div class="entry_info"> 
			
			<p class="meta">
			
				<cite><?php printf( __( '%s', 'framework' ), sprintf( '%s', get_comment_author_link() ) ); ?></cite>
				
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s', 'framework' ), get_comment_date(),  get_comment_time() ); ?></a>
				- <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				
				<?php edit_comment_link( __( '(Edit)', 'framework' ), ' ' );
			?>
			</p>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<?php _e( '<p><em>Your comment is awaiting moderation.</em></p>', 'framework' ); ?>
			<?php endif; ?>
			
			<?php comment_text(); ?>
		
		<!-- END .entry_info -->
		</div>
	
	<!-- END Comment Article -->
	</article>
		
	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<article <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<p><?php _e( 'Pingback:', 'framework' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'framework'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Close HTML5 Comments
/*-----------------------------------------------------------------------------------*/

function tj_comment_close() {
	echo '';
}

/*-----------------------------------------------------------------------------------*/
/*	HTML5 Comment Form
/*-----------------------------------------------------------------------------------*/

function tj_fields($fields) {

	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	
	$fields =  array(
		'author' => '<p><label for="author">' . __( 'Name', 'framework' ) . ' ' . ( $req ? '*' : '' ) . '</label> ' .
		'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
		'email'  => '<p><label for="email">' . __( 'Email', 'framework' ) . ' ' . ( $req ? '*' : '' ) . '</label> ' . 
		'<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
		'url'    => '<p><label for="url">' . __( 'Website', 'framework' ) . '</label>' .
		'<input id="url" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
	);
	
	return $fields;
}
add_filter('comment_form_default_fields','tj_fields');	

/*-----------------------------------------------------------------------------------*/
/* Custom Walker
/*-----------------------------------------------------------------------------------*/

class Gallery_Walker extends Walker_Category {
    function start_el( &$output, $category, $depth = 0, $args = array(), $current_object_id = 0 ) {
            extract($args);

            $cat_name = esc_attr( $category->name );
            $cat_name = apply_filters( 'list_cats', $cat_name, $category );
            $link = '<a href="' . esc_attr( get_term_link($category) ) . '" ';
            $link .= 'data-filter="' . urldecode($category->slug) . '" ';
            if ( $use_desc_for_title == 0 || empty($category->description) )
                    $link .= 'title="' . esc_attr( sprintf(__( 'View all posts filed under %s', 'framework' ), $cat_name) ) . '"';
            else
                    $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
            $link .= '>';
            $link .= $cat_name . '</a>';

            if ( !empty($feed_image) || !empty($feed) ) {
                    $link .= ' ';

                    if ( empty($feed_image) )
                            $link .= '(';

                    $link .= '<a href="' . get_term_feed_link( $category->term_id, $category->taxonomy, $feed_type ) . '"';

                    if ( empty($feed) ) {
                            $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s', 'framework' ), $cat_name ) . '"';
                    } else {
                            $title = ' title="' . $feed . '"';
                            $alt = ' alt="' . $feed . '"';
                            $name = $feed;
                            $link .= $title;
                    }

                    $link .= '>';

                    if ( empty($feed_image) )
                            $link .= $name;
                    else
                            $link .= "<img src='$feed_image'$alt$title" . ' />';

                    $link .= '</a>';

                    if ( empty($feed_image) )
                            $link .= ')';
            }

            if ( !empty($show_count) )
                    $link .= ' (' . intval($category->count) . ')';

            if ( !empty($show_date) )
                    $link .= ' ' . gmdate('Y-m-d', $category->last_update_timestamp);

            if ( 'list' == $args['style'] ) {
                    $output .= "\t<li";
                    $class = 'cat-item cat-item-' . $category->term_id;
                    if ( !empty($current_category) ) {
                            $_current_category = get_term( $current_category, $category->taxonomy );
                            if ( $category->term_id == $current_category )
                                    $class .=  ' current-cat';
                            elseif ( $category->term_id == $_current_category->parent )
                                    $class .=  ' current-cat-parent';
                    }
                    $output .=  ' class="' . $class . '"';
                    $output .= ">$link\n";
            } else {
                    $output .= "\t$link<br />\n";
            }
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Remove WP Version For Security
/*-----------------------------------------------------------------------------------*/

remove_action( 'wp_head', 'wp_generator' );

/*-----------------------------------------------------------------------------------*/
/*	Add Theme Version Meta
/*-----------------------------------------------------------------------------------*/

if ( ! function_exists( 'tj_theme_version' ) ) {
	function tj_theme_version() {
	    if( function_exists( 'wp_get_theme' ) ) {
			$theme_details = wp_get_theme();
			$theme_name = $theme_details->Name;
			$theme_version = $theme_details->Version;
		}else {
			$theme_data = get_theme_data( get_stylesheet_directory() . '/style.css' );
			$theme_name = $theme_data['Name'];
			$theme_version = $theme_data['Version'];
		}
		
		echo '<meta name="generator" content="' . $theme_name . ' ' . $theme_version .' - ' . __('ThemeJug.com','framework') . '" />' . "\n";
		
	}
	add_action('wp_head', 'tj_theme_version', 1);
}