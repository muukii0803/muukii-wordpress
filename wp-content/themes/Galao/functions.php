<?php

/*-----------------------------------------------------------------------------------
/*	Galao Theme Functions - v.1.0
-----------------------------------------------------------------------------------*/

if ( ! isset( $content_width ) ) $content_width = 670;

/*-----------------------------------------------------------------------------------*/
/*	Theme Setup
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'themejug_theme_setup' ) ) {

	function themejug_theme_setup(){
	    
	    /* Theme Translations */
	    
	    load_theme_textdomain('framework', TEMPLATEPATH . '/languages');
	    $locale = get_locale();
	    $locale_file = TEMPLATEPATH . "/languages/$locale.php";
	    if ( is_readable( $locale_file ) )
	    	require_once( $locale_file );
	    
	    /* WP Menus */
	    
		register_nav_menu('primary-menu', __('Primary Menu', 'framework'));
		register_nav_menu('secondary-menu', __('Secondary Menu', 'framework'));
	    
	    /* Post Formats */
	    
	    add_theme_support( 
	        'post-formats', 
	        array(
	        	'audio',
	            'gallery',
	            'image',
	            'link',
	            'quote',
	            'video',
	            'aside'
	        ) 
	    );
	    
	    /* WP Thumbnails */
	    add_theme_support( 'post-thumbnails', array( 'post','portfolio' )  );
	   	set_post_thumbnail_size( 60, 60, true ); // Thumbnails
		add_image_size( 'blog-half', 300, 175, true); // Blog Thumbnail - Cropped
		add_image_size( 'blog-full', 680, '', false); // Blog - Full
		add_image_size( 'slider', 940, 650, true); // Projects - Slider
		add_image_size( 'project-thumb', 56, 56, true); // Projects - Sidebar widgets
		add_image_size( 'project-half', 460, 460, true); // Projects - Half
		add_image_size( 'project-full', 940, '', false); // Projects - Full
	    
	    /* Feed Links */
	    
	    add_theme_support( 'automatic-feed-links' );
	    
	}
	
}
add_action('after_setup_theme', 'themejug_theme_setup');

/*-----------------------------------------------------------------------------------*/
/*	Register Sidebars
/*-----------------------------------------------------------------------------------*/

if( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Blog Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}

/*-----------------------------------------------------------------------------------*/
/*	Custom Gravatar
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_custom_gravatar' ) ) {
    function tj_custom_gravatar( $avatar_defaults ) {
        $tj_avatar = get_template_directory_uri() . '/img/gravatar.png';
        $avatar_defaults[$tj_avatar] = 'Custom Gravatar (/img/gravatar.png)';
        return $avatar_defaults;
    }
    
    add_filter( 'avatar_defaults', 'tj_custom_gravatar' );
}

/*-----------------------------------------------------------------------------------*/
/*	Change Default Excerpt Length
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_excerpt_length' ) ) {
    function tj_excerpt_length( $length ) {
   		return 35; 
    }
    
    add_filter('excerpt_length', 'tj_excerpt_length', 999);
}

/*-----------------------------------------------------------------------------------*/
/*	Configure Excerpt String
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_excerpt_more' ) ) {
    function tj_excerpt_more($excerpt) {
    	return str_replace('[...]', '...', $excerpt); 
    }
    
    add_filter('wp_trim_excerpt', 'tj_excerpt_more');
}

/*-----------------------------------------------------------------------------------*/
/*	Enable Page Excerpts
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_page_excerpt' ) ) {
	function tj_page_excerpt() {
	     add_post_type_support( 'page', 'excerpt' );
	}
	add_action( 'init', 'tj_page_excerpt' );
}

/*-----------------------------------------------------------------------------------*/
/*	Register and load front end CSS
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_enqueue_styles' ) ) {
	function tj_enqueue_styles() { 
	
		wp_register_style( 'tj-style', get_template_directory_uri() . '/style.css', array(), '', 'all' );
		wp_register_style( 'response', get_template_directory_uri() . '/css/responsive.css', array(), '', 'all' );
		wp_register_style( 'fonts', 'http://fonts.googleapis.com/css?family=Raleway:400,700,500|Karla:400,700', array(), '', 'all' );
		wp_register_style( 'fontAwesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '', 'all' );
		
		wp_enqueue_style( 'tj-style' );
		wp_enqueue_style( 'fonts' );
		wp_enqueue_style( 'fontAwesome' );
		
		$responsive_switch = of_get_option('theme_responsive');
		if ( !empty($responsive_switch) && $responsive_switch == 'on' ) {
			wp_enqueue_style( 'response' );
		}elseif ( empty($responsive_switch) ) {
			wp_enqueue_style( 'response' );
		}
	}
	
	add_action('wp_enqueue_scripts', 'tj_enqueue_styles');
}

/*-----------------------------------------------------------------------------------*/
/*	Register and load front end JS
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_enqueue_scripts' ) ) {
    function tj_enqueue_scripts() {
		
		// Comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
		wp_register_script('superfish', get_template_directory_uri() . '/js/superfish.js', 'jquery', '1.0', TRUE);
		wp_register_script('tj_custom', get_template_directory_uri() . '/js/jquery.custom.js', array('jquery', 'superfish'), '1.0', TRUE);
		wp_register_script('jPlayer', get_template_directory_uri() . '/js/jquery.jplayer.min.js', 'jquery', '2.1', TRUE);
		wp_register_script('fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', 'jquery', '1.0', TRUE);
		wp_register_script('slider', get_template_directory_uri().'/js/jquery.flexslider-min.js', 'jquery', TRUE);
		wp_register_script('mosaic', get_template_directory_uri().'/js/mosaic.1.0.1.min.js', 'jquery', TRUE);
		wp_register_script('validation', 'http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js', 'jquery');
		wp_register_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui-1.8.5.custom.min.js', 'jquery');
		wp_register_script('isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', 'jquery', '1.5.03', TRUE);
        
        // Enqueue our scripts
    	wp_enqueue_script('jquery');
    	wp_enqueue_script('superfish');
    	wp_enqueue_script('mosaic');
    	wp_enqueue_script('tj_custom');
    	wp_enqueue_script('jquery-ui');
    	wp_enqueue_script('fitvids');
    	
    	if( is_page_template( 'template-portfolio-filtered.php' ) ) {
    	    wp_enqueue_script('isotope');
    	}
    	
    	// load jPlayer on appropriate pages
    	if( is_home() || ('portfolio' == get_post_type()) || has_post_format('video') || has_post_format('audio') || is_search() ) {
    	    wp_enqueue_script('jPlayer');
    	}
    	
    	// load FlexSlider on appropriate pages
    	if( is_home() || is_page_template( 'template-home.php' ) || is_author() || is_page_template( 'template-portfolio-paginated.php' ) || is_page_template( 'template-portfolio-filtered.php' ) || ( 'portfolio' == get_post_type() ) || has_post_format('gallery') || is_search() ) {
    	    wp_enqueue_script('slider');
    	}
    	
    	// load Validation on the template-contact page or if the contact widget is active
    	if( is_page_template( 'template-contact.php' ) ) {
    	    wp_enqueue_script('validation');
    	}
    	
        // loads the javascript required for threaded comments
        if( is_singular() ) { 
        	wp_enqueue_script( 'comment-reply' );
        }  
    }
    
    add_action('wp_enqueue_scripts', 'tj_enqueue_scripts');
}

/*-----------------------------------------------------------------------------------*/
/* Validate For Contact Form 
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_contact_validate' ) ) {
    function tj_contact_validate() {
    	if (is_page_template('template-contact.php') ) { ?>
    		<script type="text/javascript">
    			jQuery(document).ready(function(){
    				jQuery("#contact").validate();
    			});
    		</script>
    	<?php }
    }
    
    add_action('wp_head', 'tj_contact_validate');
}

/*-----------------------------------------------------------------------------------*/
/* Homepage Flexlider - Testimonials
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_testimonial' ) ) {
    function tj_testimonial() {
    	if (is_page_template('template-portfolio-paginated.php')) { ?>
			<script type="text/javascript" charset="utf-8">
			  $(window).load(function() {
				$('#tj-testimonials').flexslider({
				animation: 'fade',
				slideshow: true,
				directionNav: false,
				controlNav: false,
				keyboardNav: true,
				mousewheel: false,
				touch: true,
				smoothHeight: true
				});
			  });
			</script>
    	<?php }
    }
    
    add_action('wp_head', 'tj_testimonial');
}

/*-----------------------------------------------------------------------------------*/
/*	Register and load admin javascript
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_admin_js' ) ) {
    function tj_admin_js($hook) {
    	if ($hook == 'post.php' || $hook == 'post-new.php') {
    		wp_register_script('tj-admin', get_template_directory_uri() . '/js/jquery.custom.admin.js', 'jquery');
    		wp_enqueue_script('tj-admin');
    	}
    }
    
    add_action('admin_enqueue_scripts','tj_admin_js',10,1);
}

/*-----------------------------------------------------------------------------------*/
/*	Add Browser Detection Body Class
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'tj_browser_body_class' ) ) {
    function tj_browser_body_class($classes) {
		global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
	
		if($is_lynx) $classes[] = 'lynx';
		elseif($is_gecko) $classes[] = 'gecko';
		elseif($is_opera) $classes[] = 'opera';
		elseif($is_NS4) $classes[] = 'ns4';
		elseif($is_safari) $classes[] = 'safari';
		elseif($is_chrome) $classes[] = 'chrome';
		elseif($is_IE){ 
			$classes[] = 'ie';
			if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version)) $classes[] = 'ie'.$browser_version[1];
		} else $classes[] = 'unknown';
	
		if($is_iphone) $classes[] = 'iphone';
		return $classes;
    }
    
    add_filter('body_class','tj_browser_body_class');
}

/*-----------------------------------------------------------------------------------*/
/*	Custom Login Logo Support
/*-----------------------------------------------------------------------------------*/

if( !function_exists( 'tj_custom_login_logo' ) ) {
    function tj_custom_login_logo() {
		if (of_get_option('custom_login_logo')) {
		$loginLogo = stripslashes(of_get_option('custom_login_logo'));
        echo '<style type="text/css">h1 a { background-image:url('. $loginLogo .') !important; background-size: auto auto !important; }</style>';		    
		}else{
        echo '<style type="text/css">h1 a { background-image:url('.get_template_directory_uri().'/img/logo-login.png) !important; background-size: auto auto !important; }</style>';
       }
        
    }
    
    add_action('login_head', 'tj_custom_login_logo');
}

/*-----------------------------------------------------------------------------------*/
/*	Load Widgets
/*-----------------------------------------------------------------------------------*/

// Add the Flickr Photos Custom Widget
include("functions/widget-flickr.php");

// Add the Custom Video Widget
include("functions/widget-video.php");
 
// Add Custom Portfolios Widget
include('functions/widget-recent-portfolios.php');

// Add the portfolio meta
include('functions/theme-postmeta.php');

// Add the page meta
include('functions/theme-pagemeta.php');

// Add the portfolio meta
include('functions/theme-portfoliometa.php');

// Add the testimonial meta
include('functions/theme-testimonialmeta.php');

// Add the post types
include("functions/theme-posttypes.php");

/*-----------------------------------------------------------------------------------*/
/*	Themepath
/*-----------------------------------------------------------------------------------*/

define('tj_FILEPATH', TEMPLATEPATH);
define('tj_DIRECTORY', get_template_directory_uri());

/*-----------------------------------------------------------------------------------*/
/*	Load Theme Functions
/*-----------------------------------------------------------------------------------*/

require_once (tj_FILEPATH . '/functions/theme-functions.php');

/*-----------------------------------------------------------------------------------*/
/*	Load Theme Options Framewwork
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/admin/' );
	require_once dirname( __FILE__ ) . '/admin/options-framework.php';
}