/*-----------------------------------------------------------------------------------*/
/*	ThemeJug - Theme Functions - v.1.0
/*-----------------------------------------------------------------------------------*/
 
jQuery(window).ready(function() {


	/*-----------------------------------------------------------------------------------*/
	/*	Mosaic
	/*-----------------------------------------------------------------------------------*/
	
	if( jQuery().mosaic ) {
	    
	    jQuery(function() {	
		
			jQuery('.fade').mosaic();
				
		});
		
	}
	
	/*-----------------------------------------------------------------------------------*/
	/*	Superfish Settings - http://users.tpg.com.au/j_birch/plugins/superfish/
	/*-----------------------------------------------------------------------------------*/
	if( jQuery().superfish ) {
	
	    jQuery(function() {	
	    
			jQuery('header nav ul').superfish({
				delay: 0,
				animation: {opacity:'show'},
				speed: 'fast',
				autoArrows: true,
				dropShadows: false
			});
		
		});
	}

	/*-----------------------------------------------------------------------------------*/
	/*	Responsive Videos - avec FitVids.js
	/*-----------------------------------------------------------------------------------*/
	
	jQuery('#primary, .fullwidth').fitVids();
	
	/*-----------------------------------------------------------------------------------*/
	/*	Portfolio Filter
	/*-----------------------------------------------------------------------------------*/
	
	if( jQuery().isotope ) {
	
		jQuery(function() {
	
	            var container = jQuery('.isotope'),
	                optionFilter = jQuery('#sort-by'),
	                optionFilterLinks = optionFilter.find('a');
	            
	            optionFilterLinks.attr('href', '#');
	            
	            optionFilterLinks.click(function(){
	            
	                var selector = jQuery(this).attr('data-filter');
	                container.isotope({ 
	                    filter : '.' + selector, 
	                    itemSelector : '.isotope-item',
	                    layoutMode : 'fitRows',
	                    animationEngine : 'best-available'
	                    
	                });
	                
	                optionFilterLinks.removeClass('active');
	                jQuery(this).addClass('active');
	                return false;
	                
	            });
	            
		    });
	    
	}
	
});