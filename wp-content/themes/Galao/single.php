<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
	
	<?php if (have_posts()) : $countposts = 0; while (have_posts()) : the_post(); ?>
							
		<!--BEGIN Article -->
		<article id="post-<?php the_ID(); ?>" <?php if ($countposts == 1) { post_class('first clearfix');} else{ post_class('clearfix'); } ?>>
		
			<?php 
				$format = get_post_format();
				if( false === $format ) { $format = 'standard'; }
				get_template_part( 'includes/' . $format ); 
			 ?>		
			
			<div class="entry-content">
									    
				<?php the_content(); ?>
				
				<div class="post-meta">
				
					<p><?php _e('Posted: ','framework'); ?><?php the_time(get_option('date_format')); ?> <?php _e('By: ','framework'); ?><?php the_author_posts_link(); ?></p>
					
					<p><?php _e('With: ','framework'); ?><a href="<?php comments_link(); ?>"><?php comments_number( '' . __('0 Comments', 'framework') . '', '' . __('1 Comment', 'framework') . '', '' . __('% Comments', 'framework') . '' ); ?></a></p>
					
				   <?php the_tags('<p class="tags">&#35;',' &#35;', '</p>' ); ?>
				   
				</div>
				
			</div>		
			
		<!--END Article -->  
		</article>
			
		<?php comments_template('', true); ?>
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!--BEGIN #404 Article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>> 		
		
			<h2 class="entry-title"><?php _e('Error 404 - Not Found', 'framework') ?></h2>
			<p><?php _e("Sorry, but you are looking for something that isn't here.", "framework") ?></p>
								
		<!--END #404 Article-->
		</article>
						
	<?php endif; ?>
	
	</div>
	
	<?php get_sidebar(); ?>
	
</div>

<?php get_footer(); ?>