<?php
/*
Template Name: Page - Archives
*/
?>
<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">

	<?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
		
		<!-- BEGIN Article-->
		<article id="page-<?php the_ID(); ?>" <?php post_class('page-sidebar archives'); ?>>
				
			<div class="entry-content">
			
				<?php the_content(); ?>
		
				<h3 class="archive-title"><?php _e('Blog Archives', 'framework') ?></h3>
			
				<ul>
				<?php $post_list = get_posts('numberposts=30');
				foreach($post_list as $post) : ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title();?></a></li>
				<?php endforeach; ?>
				</ul>
			
				<h3><?php _e('Monthly Archives:', 'framework') ?></h3>
			
				<ul>
					<?php wp_get_archives('type=monthly'); ?>
				</ul>
		
				<h3><?php _e('Category Arhives:', 'framework') ?></h4>
			
				<ul>
			 		<?php wp_list_categories( 'title_li=' ); ?>
				</ul>
		
			</div>
			
		<!-- END Article -->
		</article>
		
	<?php endwhile; ?>
	
	<?php else : ?>
	
	<!--BEGIN Article -->
	<article id="page" class="page-sidebar archives">							
								
		<div class="entry-content">
			<h3><?php _e('404 - Not Found', 'framework') ?></h3>
			<p><?php _e("Sorry, but you are looking for something that isn't here.", "framework") ?></p>
		</div>
		
	<!--END Article-->
	</article>
	
	<?php endif; ?>


	</div>
	
	<?php get_sidebar(); ?>

</div>
			
<?php get_footer(); ?>