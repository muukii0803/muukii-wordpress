<?php get_header(); ?>

<?php /* Get author data */
	if(get_query_var('author_name')) :
	$curauth = get_userdatabylogin(get_query_var('author_name'));
	else :
	$curauth = get_userdata(get_query_var('author'));
	endif;
?>
		
<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
	
	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
	<?php /* If this is a category archive */ if (is_category()) { ?>
		<h2 class="archive-title"><?php printf(__('All posts in %s', 'framework'), single_cat_title('',false)); ?></h2>
	<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<h2 class="archive-title"><?php printf(__('All posts tagged %s', 'framework'), single_tag_title('',false)); ?></h2>
	<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="archive-title"><?php _e('Archive for', 'framework') ?> <?php the_time('F jS, Y'); ?></h2>
	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="archive-title"><?php _e('Archive for', 'framework') ?> <?php the_time('F, Y'); ?></h2>
	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="archive-title"><?php _e('Archive for', 'framework') ?> <?php the_time('Y'); ?></h2>
	<?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="archive-title"><?php _e('All posts by', 'framework') ?> <?php echo $curauth->display_name; ?></h2>
	<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="archive-title"><?php _e('Blog Archives', 'framework') ?></h2>
	<?php } ?>
								            
	<?php if (have_posts()) : $countposts = 0; while (have_posts()) : the_post(); $countposts++; ?>
	
		<!--BEGIN Article -->
		<article id="post-<?php the_ID(); ?>" <?php if ($countposts == 1) { post_class('first clearfix');} else{ post_class('clearfix'); } ?>>
									    						
			<div class="entry-content">
			
			<?php 
				$format = get_post_format();
				if( false === $format ) { $format = 'standard'; }
				get_template_part( 'includes/' . $format ); 
			?>
			
				<?php the_excerpt(); ?>

			</div>
			
			<?php if( $format == 'standard' || $format == 'gallery' || $format == 'image' || $format == 'video' || $format == 'audio') { ?>
			
			<div class="post-meta">
				<p><?php _e('Posted: ','framework'); ?><?php the_time(get_option('date_format')); ?> <?php _e('By: ','framework'); ?> <?php the_author(); ?></p>
				
				<p><?php _e('With: ','framework'); ?><a href="<?php comments_link(); ?>"><?php comments_number( '' . __('0 Comments', 'framework') . '', '' . __('1 Comment', 'framework') . '', '' . __('% Comments', 'framework') . '' ); ?></a></p>
				
			   <?php the_tags('<p class="tags">&#35;',' &#35;', '</p>' ); ?>
			   
			</div>
			
			<?php } ?>
		
		<!-- END Article -->
		</article>	
						
	<?php endwhile; ?>
				
		<?php if( function_exists( 'wp_pagenavi' ) ) : ?>
			<div class="pagination">
			
				<nav>
					
					<?php wp_pagenavi(); ?>
					
				</nav>	
				
			</div>
		<?php else : ?>
		
			<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				
				<div class="pagination-default">
					
					<nav>
						
						<div class="pagination-default-left"><?php next_posts_link( __( '&larr; Older posts', 'framework' ) ); ?></div>
						
						<div class="pagination-default-right"><?php previous_posts_link( __( 'Newer posts &rarr;', 'framework' ) ); ?></div>
				
					</nav>
					
				</div>
				
			<?php endif; ?>
			
		<?php endif; ?> 
				
	<?php else : ?>
	
	<!--BEGIN Article -->
	<article id="post-0" class="first clearfix">							
														
		<h3><?php _e('404 - Not Found', 'framework') ?></h3>
		
		<p><?php _e("Sorry, but you are looking for something that isn't here.", "framework") ?></p>
						
	<!--END Article-->
	</article>
				
	<?php endif; ?>
	
	</div>
	
	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>
