<!DOCTYPE html>

<!-- A ThemeJug WordPress Theme  -->
<html <?php language_attributes(); ?>>

<!-- BEGIN head -->
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=100%, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	
	<!-- Site Title -->
	<title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
	
	<!-- RSS / Pings -->
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php if (of_get_option('custom_rss')) { echo of_get_option('custom_rss'); } else { bloginfo( 'rss2_url' ); } ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	
	<!-- BEGIN wp_head -->
	<?php wp_head(); ?>
	<!-- END wp_head -->
		
</head>
<body <?php body_class(); ?>>

<div class="header-wrap clearfix">

	<header id="header" class="clearfix">
	
		<div class="logo">
		<?php if (of_get_option('plain_logo') == '1') { ?>
			<h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<?php } elseif (of_get_option('custom_logo')) { ?>
			<h1><a href="<?php echo home_url(); ?>"><img src="<?php echo stripslashes(of_get_option('custom_logo')); ?>" alt="<?php bloginfo( 'name' ); ?>"/></a></h1>
		<?php } else { ?>
			<h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>
		<?php } ?>
		</div>
		
		<!-- BEGIN .nav -->
		<?php if ( has_nav_menu( 'primary-menu' ) ) {  ?>
		
			<?php wp_nav_menu( array( 'container' => 'nav', 'fallback_cb' => 'wp_page_menu', 'theme_location' => 'primary-menu' ) ); ?>
			
		<?php } ?>
		
	</header>

</div>