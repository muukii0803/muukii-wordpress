<footer id="footer" class="clearfix"> 

	<div class="footer-inner">

		<div class="footer-left">
			<?php
			if ( of_get_option('footer_left') ) { //Show left footer text if it exists ?>
				<p><?php echo do_shortcode( stripslashes( of_get_option( 'footer_left' ) ) ); ?></p>	
			<?php }else { ?>
				<p>&copy; <?php _e('Copyright','framework'); ?>  <a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php bloginfo( 'name' ); ?>.</a></p>
				<p class="tj-credit"><a href="http://themejug.com/theme/galeo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - Premium WordPress Theme"><?php bloginfo( 'name' ); ?> Theme</a> <em>by</em> <a href="http://themejug.com" title="ThemeJug - Premium WordPress Themes">ThemeJug</a>.</p>
			<?php } ?>
		</div>
		
		<div class="footer-right">
			<?php
				if ( of_get_option('footer_right') ) { //Show right footer menu if it exists ?>
					<p class="right"><?php echo do_shortcode( stripslashes( of_get_option( 'footer_right' ) ) ); ?></p>	
			<?php }else { ?>
				<?php if ( has_nav_menu( 'secondary-menu' ) ) { ?>	
					<?php wp_nav_menu( array( 'container' => 'nav', 'container_id' => 'footer-nav', 'theme_location' => 'secondary-menu' ) ); ?>
				<?php } ?>		
			<?php } ?>	
		</div>
	
	</div>
			
</footer>

<?php echo '<!-- '.'Ran '. $wpdb->num_queries .' queries '. timer_stop(0, 2) .' seconds'.' -->'; ?>

<?php wp_footer(); ?>

</body>
</html>