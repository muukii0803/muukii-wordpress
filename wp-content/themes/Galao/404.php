<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">

		<!--BEGIN Article -->
		<article id="post-0" class="first clearfix">		
		
		<div class="entry-content">				
			
			<h1 class="entry-title"><?php _e('You hit a 404!', 'framework') ?></h1>
			
		</div>
	
		<!--END Article -->
		</article>
	
	</div>
	
	<?php get_sidebar(); ?>
		
</div>
			
<?php get_footer(); ?>