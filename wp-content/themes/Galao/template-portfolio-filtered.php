<?php
/*
Template Name: Portfolio - Filtered
*/
?>
<?php get_header(); ?>

<div id="content" class="portfolio-paginated clearfix">

	<?php tj_portfolio_welcome(); ?>

	<div id="sort-by">

		<ul>
			<li><a href="#all" data-filter="type-portfolio" class="active"><?php _e('All', 'framework'); ?></a></li>
			<?php wp_list_categories( array('title_li' => '', 'taxonomy' => 'portfolio-type', 'walker' => new Gallery_Walker() ) ); ?>

			</ul>
	</div>
	
	<div class="isotope clearfix">
		
		<?php
		
		$args = array(
		'post_type' => 'portfolio',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1
		);
		$wp_query = new WP_Query( $args );
		
		$countposts = 0;
		
		while ( $wp_query->have_posts() ) : $wp_query->the_post(); $countposts++; 
		
		$terms =  get_the_terms( $post->ID, 'portfolio-type' );
		$term_list = '';
		if( is_array($terms) ) {
			foreach( $terms as $term ) {
	
			$term_list .= urldecode($term->slug);
	
			$term_list .= ' ';
			
			}
	
		}
		
		?>
				
		<!-- BEGIN Article-->
		<article id="post-<?php the_ID(); ?>" <?php post_class("$term_list isotope-item"); ?>>
		
			<div class="mosaic-block fade">
			
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="mosaic-overlay">
					
					<div class="details">
						
						<h3 class="entry-title"><?php the_title(); ?></h3>
						
					</div>
					
				</a>
				
				<div class="mosaic-backdrop">
				
					<?php the_post_thumbnail('project-half'); ?>
					
				</div>
				
			</div>
	
		<!-- END Article -->
		</article>	
			
		<?php endwhile; wp_reset_postdata(); ?>
	
	</div>
		
</div>
	
<?php get_footer(); ?>