<?php
$testimonial_title = of_get_option('client_testimonials');

if ($testimonial_title == ''){
	$testimonial_title = __('Client Testimonials', 'framework');
}
?>

<div class="testimonial clearfix">

	<div class="testimonial-title">

		<h3 class="widget-title"><?php echo $testimonial_title; ?></h3>
		
		<div class="seperator"></div>
		
	</div>
	
	<div id="tj-testimonials" class="flexslider">
	
		<ul class="slides">
		
		<?php
		$args = array(
		'post_type' => 'testimonial',
		'orderby' => 'menu_order',
		'order' => 'DESC',
		'orderby' => 'date'
		);
		$wp_query = new WP_Query( $args );
		
	    while( $wp_query->have_posts() ) : $wp_query->the_post();
	    
	    $testimonialurl = get_post_meta($post->ID, 'tj_testimonial_url', true);
		if ( $testimonialurl !='' ){ ?>
	
			<li>                
				
				<?php the_content();?> 
				
				<h5 class="testimonial-meta"> &mdash; <a href="<?php echo $testimonialurl; ?>" target="_blank" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>"> <?php the_title(); ?></a></h5>
					
			</li>	
	
		<?php }else { ?>
	
			<li id="post-<?php the_ID(); ?>" <?php post_class('testimonial'); ?>>  
			              
				<?php the_content();?> 
				
				<h5 class="testimonial-meta"> &mdash; <?php the_title(); ?></h5>
				
			</li>			    
	
		<?php } ?>
	
		<?php endwhile; wp_reset_postdata(); ?>
	
		</ul>
	
	</div>
	
</div>

