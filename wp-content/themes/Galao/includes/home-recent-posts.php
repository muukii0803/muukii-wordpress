<div class="home-posts-fullwidth">	
	
	<div class="home-posts clearfix">
	
		<?php
			$post_title = of_get_option('show_posts_title');
			$caption = of_get_option('show_posts_caption');
			
			if ($post_title == ''){
				$post_title = __('What I Do', 'framework');
			}

			if ( !empty($post_title) ){
				echo "<div class=\"home-caption clearfix\">";
				echo "<h3 class=\"widget-title\">" . $post_title . "</h3>";
				echo "<div class=\"seperator clearfix\"></div>";
				
				if ( !empty($caption) ){
					$content = apply_filters('the_content', $caption);
					echo $content;
				}
				
				echo "</div>";
			}
			
			$query = new WP_Query();
			$query->query( array(
				'post_type' => 'page',
				'meta_key' => 'tj_feature',
				'meta_value' => 'on',
				'order' => 'ASC',
				'posts_per_page' => 3,
			    'ignore_sticky_posts' => 1,
			    'tax_query' => array(
			                        array(
			                            'taxonomy' => 'post_format',
			                            'field' => 'slug',
			                            'terms' => array( 'post-format-aside', 'post-format-link', 'post-format-quote' ),
			                            'operator' => 'NOT IN'
			                        )
			                    )
			        ));
				
		    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 
		    $external_thumb_url = get_post_meta($post->ID, 'tj_feature-url', true);
		    ?>
		    
				<!-- BEGIN Article-->
				<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
				
				<?php if ( $external_thumb_url ) : ?>
					
					<div class="blog-hero"><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php echo $external_thumb_url; ?>"><?php the_post_thumbnail('blog-half'); ?></a></div>
				
				<?php else: ?>
				
					<div class="blog-hero"><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-half'); ?></a></div>
							
				<?php endif; ?>
					
					<div class="entry-content">
										
						<h4 class="entry-title"><a rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						
						<?php the_excerpt(); ?>
			
					</div>	
									 
				<!-- END Article -->	                   
				</article>
							
		<?php endwhile; endif; wp_reset_postdata(); ?>
	
	</div>
	
</div>