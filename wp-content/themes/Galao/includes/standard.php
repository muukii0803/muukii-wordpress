<?php if( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
	<div class="blog-hero"><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-full'); ?></a></div>
<?php } ?>

<?php if( is_single() ) { ?>

	<h1 class="entry-title"><?php the_title(); ?></h1>
   
<?php } else { ?>
    <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>"><?php the_title(); ?></a></h3>    
<?php } ?>