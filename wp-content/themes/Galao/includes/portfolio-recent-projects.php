<div class="portfolio-related clearfix">

	<div class="portfolio-related-title">

		<h4 class="widget-title"><?php _e('Related Projects', 'framework'); ?></h4>
	
		<div class="seperator"></div>
		
	</div>
	
	<?php
		$show_related_projects_count = of_get_option('show_related_projects_count');
		
		if ($show_related_projects_count == ''){
			$show_related_projects_count = "2";
		}
	
		$args = array(
		'post_type' => 'portfolio',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => $show_related_projects_count,
		'post__not_in' => array($post->ID),
		'paged' => get_query_var('paged')
		);
		$wp_query = new WP_Query( $args );
	
	    while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
	    		
		<!-- BEGIN Article-->
		<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
		
			<div class="mosaic-block fade">
			
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="mosaic-overlay">
					
					<div class="details">
						
						<h3 class="entry-title"><?php the_title(); ?></h3>
						
					</div>
					
				</a>
				
				<div class="mosaic-backdrop">
				
					<?php the_post_thumbnail('project-half'); ?>
					
				</div>
				
			</div>
	
		<!-- END Article -->
		</article>
				
		<?php endwhile; wp_reset_postdata(); ?>
	
	</div>