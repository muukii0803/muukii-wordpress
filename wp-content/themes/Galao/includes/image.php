<?php if( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
	<div class="blog-hero"><a title="<?php printf(__('Permanent Link to %s', 'framework'), get_the_title()); ?>" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-full'); ?></a></div>
<?php } ?>

<?php if( is_single() ) { ?>
    
    <h2><?php the_title(); ?></h2>
	    
<?php } ?>