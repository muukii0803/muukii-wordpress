<?php $link = get_post_meta($post->ID, 'tj_link_url', TRUE); ?>

<div class="link">

	<?php if( is_singular() ) { ?>
	    <h2 class="entry-title"><a href="<?php echo $link; ?>" target="_blank" title="<?php the_title(); ?>"><?php the_title(); ?><i class="icon-link"></i></a></h2>
	<?php } else { ?>
		<h3 class="entry-title"><a href="<?php echo $link; ?>" target="_blank" title="<?php the_title(); ?>"><i class="icon-link"></i> <?php the_title(); ?></a>
		<span class="link-title"> &mdash; <a href="<?php echo $link; ?>"><?php echo $link; ?></a></span></h3>  
	<?php } ?>

</div>
