<?php $quote = get_post_meta($post->ID, 'tj_quote', TRUE); ?>

<div class="quote">
	<h3 class="entry-title">
		<blockquote>
			"<?php echo $quote; ?>"
			<span class="quote-title">&mdash; <?php the_title(); ?></span>
		</blockquote>
	</h3>
</div>