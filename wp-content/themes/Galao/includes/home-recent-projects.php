<?php
	$home_project_count = of_get_option('home_project_count');
	$project_title = of_get_option('project_title');
	$project_caption = of_get_option('show_projects_caption');
	
	if ($project_title == ''){
		$project_title = __('Recent Projects', 'framework');
	}
	
	if ($home_project_count == ''){
		$home_project_count = "4";
	}
	
	$args = array(
	'post_type' => 'portfolio',
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'posts_per_page' => $home_project_count,
	'paged' => get_query_var('paged')
	
	);
	$wp_query = new WP_Query( $args );
	
	echo '<div class="home-projects clearfix">';
		
	if ( !empty($project_title) ){
		echo "<div class=\"home-caption clearfix\">";
		echo "<h3 class=\"widget-title\">" . $project_title . "</h3>";
		echo "<div class=\"seperator clearfix\"></div>";
		
		if ( !empty($project_caption) ){
			$project_content = apply_filters('the_content', $project_caption);
			echo $project_content;
		}
		
		echo "</div>";
	}

    while( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
		
	<!-- BEGIN Article-->
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
	
		<div class="mosaic-block fade">
		
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="mosaic-overlay">
				
				<div class="details">
					
					<h3 class="entry-title"><?php the_title(); ?></h3>
					
				</div>
				
			</a>
			
			<div class="mosaic-backdrop">
			
				<?php the_post_thumbnail('project-half'); ?>
				
			</div>
			
		</div>

	<!-- END Article -->
	</article>	
			
	<?php endwhile; wp_reset_postdata(); ?>
	
</div>