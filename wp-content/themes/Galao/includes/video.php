<?php
$embed = get_post_meta($post->ID, 'tj_video_embed', true);
if( !empty($embed) ) {
	echo "<div class='video'>";
    echo stripslashes(htmlspecialchars_decode($embed));
    echo "</div>";
} else {
	$type = get_post_type( $post->ID );
    if ($type === "portfolio") {
       tj_video($post->ID, 940, $type);
    }else{
     	tj_video($post->ID, 940, $type);
    }
}
?>

<?php if( is_single() ) { ?>
    
    <h2><?php the_title(); ?></h2>
	    
<?php } ?>