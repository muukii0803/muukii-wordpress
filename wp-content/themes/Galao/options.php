<?php

/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'galao' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'framework'),
		'two' => __('Two', 'framework'),
		'three' => __('Three', 'framework'),
		'four' => __('Four', 'framework'),
		'five' => __('Five', 'framework')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'framework'),
		'two' => __('Pancake', 'framework'),
		'three' => __('Omelette', 'framework'),
		'four' => __('Crepe', 'framework'),
		'five' => __('Waffle', 'framework')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/img/';

	$options = array();
	
	/*-----------------------------------------------------------------------------------*/
	/* Front Page - Options
	/*-----------------------------------------------------------------------------------*/
		
	$options[] = array( 'name' => __('Front Page', 'framework'),
						'type' => 'heading');
						
	$wp_editor_settings = array( 'wpautop' => true, // Default
								 'textarea_rows' => 5,
								 'tinymce' => array( 'plugins' => 'wordpress' ));
								 
	$options[] = array( 'name' => __('Front Page Message', 'framework'),
						'desc' => __( 'The message on the homepage template.', 'framework'),
						'id' => 'hi_message',
						'std' => '<h3>A Beautiful & Responsive Portfolio Theme</h3> You can customize this in the <a href="#">theme options</a>.',
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
	$options[] = array( 'name' => __('Featured Work Banner', 'framework'),
						'desc' => __('Feature An Item Of Work (940x450)', 'framework'),
						'id' => 'tj_feature_image',
						'type' => 'upload');
						
	$options[] = array( 'name' => __('Featured Work Banner Height Limit', 'framework'),
						'desc' => __('Set the maximum height of the featured image. (Pixels)', 'framework'),
						'id' => 'tj_feature_image_height',
						'std' => '550',
						'type' => 'text');
						
	$options[] = array( 'name' => __('Featured Work Banner External URL', 'framework'),
						'desc' => __('Link the featured work banner to a custom URL. (Prefix with: http://)', 'framework'),
						'id' => 'tj_feature_image_link',
						'std' => '',
						'type' => 'text');
						
	$options[] = array( 'name' => __('Enable Projects', 'framework'),
						'desc' => __('Shows your latest projects on the homepage template.', 'framework'),
						'id' => 'show_projects',
						'std' => '1',
						'type' => 'checkbox');
						
	$options[] = array( 'name' => __('Project Title', 'framework'),
						'desc' => __('The title used above the blog posts.', 'framework'),
						'id' => 'project_title',
						'std' => __('Projects', 'framework'),
						'type' => 'text');
						
	$options[] = array( 'name' => __('Project Caption', 'framework'),
						'desc' => __( 'The message on the homepage template.', 'framework'),
						'id' => 'show_projects_caption',
						'std' => 'A selection of the latest work to leave the studio, like what you see? Come <a href="http://twitter.com/themejug">say hello</a> and drop by for a coffee and chat.',
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
	$options[] = array( 'name' => __('Latest Projects To Show', 'framework'),
						'desc' => __('How many projects to show on the homepage', 'framework'),
						'id' => 'home_project_count',
						'std' => __('4', 'framework'),
						'type' => 'text');
						
	$options[] = array( 'name' => __('Featured Pages', 'framework'),
						'desc' => __('Shows your recent blog posts', 'framework'),
						'id' => 'show_posts',
						'std' => '1',
						'type' => 'checkbox');
						
	$options[] = array( 'name' => __('Featured Pages Title', 'framework'),
						'desc' => __('The title used above the blog posts.', 'framework'),
						'id' => 'show_posts_title',
						'std' => __('What I Do', 'framework'),
						'type' => 'text');
						
	$options[] = array( 'name' => __('Featured Pages Caption', 'framework'),
						'desc' => __( 'The message on the homepage template.', 'framework'),
						'id' => 'show_posts_caption',
						'std' => 'We are proud makers of <a href="http://themejug.com/">premium WordPress themes</a>, you, yes you can edit this in the theme options - should you wish.',
						'type' => 'editor',
						'settings' => $wp_editor_settings );
						
	/*-----------------------------------------------------------------------------------*/
	/* Style Options - Options
	/*-----------------------------------------------------------------------------------*/
																	
	$options[] = array( 'name' => __('Styling', 'framework'),
						'type' => 'heading');
						
	$options[] = array( 'name' => __('Enable Plain Text Logo', 'framework'),
						'desc' => __('Use a text based logo, rather then an image, un-tick if you upload your own.', 'framework'),
						'id' => 'plain_logo',
						'std' => '0',
						'type' => 'checkbox');							
				
	$options[] = array( 'name' => __('Upload Your Custom Logo', 'framework'),
						'desc' => __('Upload your logo.', 'framework'),
						'id' => 'custom_logo',
						'type' => 'upload');
						
	$options[] = array( 'name' => __('Upload Your Favicon', 'framework'),
						'desc' => __('Upload your favicon.', 'framework'),
						'id' => 'favicon_uploader',
						'type' => 'upload');
						
	$options[] = array( 'name' => __('Upload A iPhone/iPad Icon', 'framework'),
						'desc' => __('This will display a nice icon on your device if you "save to homescreen" on your iPhone/iPad (Size: 243x240 pixels).', 'framework'),
						'id' => 'iphone_ico_uploader',
						'type' => 'upload');
						
	$options[] = array( 'name' => __('Upload A Custom Login Logo', 'framework'),
						'desc' => __('Upload an admin panel login logo.', 'framework'),
						'id' => 'custom_login_logo',
						'type' => 'upload');					
						
	$options[] = array( 'name' => __('Link Color', 'framework'),
						'desc' => __('Change the color of your hyperlinks.', 'framework'),
						'id' => 'link_color',
						'std' => '#000000', 
						'type' => 'color');
						
	$options[] = array( 'name' => __('Link "Hover" Color', 'framework'),
						'desc' => __('Change the color of your hyperlinks when the user hovers over.', 'framework'),
						'id' => 'link_hover_color',
						'std' => '#fe6330', 
						'type' => 'color');
						
	$options[] = array( 'name' => __('Heading Color', 'framework'),
						'desc' => __('Change the color of headings.', 'framework'),
						'id' => 'heading_color',
						'std' => '#000000', 
						'type' => 'color');
						
	$options[] = array( 'name' => __('Body Text Color', 'framework'),
						'desc' => __('Change the color of site text.', 'framework'),
						'id' => 'text_color',
						'std' => '#494949', 
						'type' => 'color');
						
	$options[] = array( 'name' => __('Background Color', 'framework'),
						'desc' => __('Change the background color of your website.', 'framework'),
						'id' => 'bg_color',
						'std' => '#FAFAFA', 
						'type' => 'color');
						
	$options[] = array( 'name' => __('Upload A Background Image', 'framework'),
						'desc' => __('Upload a background image to your website.', 'framework'),
						'id' => 'bg_image',
						'type' => 'upload');
						
	$options[] = array( 'name' => __('Background Image Repeat', 'framework'),
						'desc' => __('.', 'framework'),
						'id' => 'bg_image_repeat',
						'type' => 'select',
							'options' => array(
								'' => '',
								'no-repeat' => 'No Repeat',
								'repeat' => 'Repeat',
								'repeat-x' => 'Repeat Horizontally',
								'repeat-y' => 'Repeat Vertically'
							)
						);
						
	$options[] = array( 'name' => __('Background Image Position', 'framework'),
						'desc' => __('.', 'framework'),
						'id' => 'bg_image_position',
						'type' => 'select',
							'options' => array(
								'' => '',
								'left' => 'Left',
								'right' => 'Right',
								'center' => 'Centered',
								'fullscreen' => 'Fullscreen'
							)
						);				
						
	$options[] = array( 'name' => __('Custom CSS', 'framework'),
						'desc' => __('Add your custom CSS to the theme.', 'framework'),
						'id' => 'custom_css',
						'std' => '',
						'type' => 'textarea');
						
	$options[] = array( 'name' => __('Toggle Responsive Design', 'framework'),
						'desc' => __('', 'framework'),
						'id' => 'theme_responsive',
						'std' => '1',
						'type' => 'select',
							'options' => array(
								'on' => 'On',
								'off' => 'Off'
							)
						);					
					
	/*-----------------------------------------------------------------------------------*/
	/* General - Options
	/*-----------------------------------------------------------------------------------*/
																		
	$options[] = array( 'name' => __('General', 'framework'),
						'type' => 'heading');
							
	$options[] = array( 'name' => __('Your Email Address', 'framework'),
						'desc' => __('Used in the contact form', 'framework'),
						'id' => 'custom_email',
						'std' => __('', 'framework'),
						'type' => 'text');
						
	$options[] = array( 'name' => __('Footer Text (Left)', 'framework'),
						'desc' => __('Add some text to the left side of the footer', 'framework'),
						'id' => 'footer_left',
						'std' => __('', 'framework'),
						'type' => 'textarea'); 
						
	$options[] = array( 'name' => __('Footer Text (Right)', 'framework'),
						'desc' => __('Add some text to the right side of the footer, you can also <a href="nav-menus.php">add a secondary menu</a>', 'framework'),
						'id' => 'footer_right',
						'std' => __('', 'framework'),
						'type' => 'textarea');	
						
	/*-----------------------------------------------------------------------------------*/
	/* Blog - Options
	/*-----------------------------------------------------------------------------------*/
																		
	$options[] = array( 'name' => __('Blog', 'framework'),
						'type' => 'heading');
							
	$options[] = array( 'name' => __('Feedburner URL', 'framework'),
						'desc' => __('Your RSS feed.', 'framework'),
						'id' => 'custom_rss',
						'std' => __('', 'framework'),
						'type' => 'text');
	
	$options[] = array( 'name' => __('Use Post Excerpts', 'framework'),
						'desc' => __('Display the excerpt snippet below blog post titles, if not selected the full blog content will be displayed.', 'framework'),
						'id' => 'use_excerpt',
						'std' => 'excerpt',
						'type' => 'select',
							'options' => array(
								'excerpt' => 'The Excerpt',
								'content' => 'The Content'
							)
						);	 
						
	/*-----------------------------------------------------------------------------------*/
	/* Portfolio - Options
	/*-----------------------------------------------------------------------------------*/
																		
	$options[] = array( 'name' => __('Portfolio', 'framework'),
						'type' => 'heading');
						
	$wp_editor_settings = array( 'wpautop' => true, // Default
								 'textarea_rows' => 5,
								 'tinymce' => array( 'plugins' => 'wordpress' ));
	
	$options[] = array( 'name' => __('Portfolio Pagination', 'framework'),
						'desc' => __('If you use the "Paginated Portfolio" page template, you can set how many projects to show per page', 'framework'),
						'id' => 'project_count',
						'std' => '4',
						'type' => 'text');

	$options[] = array( 'name' => __('Single Portfolio - Show Related Portfolios', 'framework'),
						'desc' => __('Shows related portfolio items below a single project post.', 'framework'),
						'id' => 'show_related_projects',
						'std' => '1',
						'type' => 'checkbox');	
	
	$options[] = array( 'name' => __('Single Portfolio - Related Portfolio Count', 'framework'),
						'desc' => __('How many related projects to show.', 'framework'),
						'id' => 'show_related_projects_count',
						'std' => '2',
						'type' => 'text');
						
	$options[] = array( 'name' => __('Single Portfolio - Exclude Featured Image', 'framework'),
						'desc' => __('Exclude the featured image from portfolio images and sliders.', 'framework'),
						'id' => 'exclude_featured_image',
						'std' => '0',
						'type' => 'checkbox');
						
	$options[] = array( 'name' => __('Enable Testimonials', 'framework'),
						'desc' => __('Shows your client testimonials on the homepage template, you need to <a href="post-new.php?post_type=testimonial">create testimonial items first</a>', 'framework'),
						'id' => 'show_testimonails',
						'std' => '0',
						'type' => 'checkbox');
						
	$options[] = array( 'name' => __('Testimonial Title', 'framework'),
						'desc' => __('The title used above the portfolio posts on the homepage template.', 'framework'),
						'id' => 'client_testimonials',
						'std' => __('Client Testimonials', 'framework'),
						'type' => 'text');
			
	return $options;
}