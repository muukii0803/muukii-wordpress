<?php get_header(); ?>

<?php 
// Get Portfolio Details
$portfolioClient = get_post_meta($post->ID, 'tj_portfolio_client', true);
$portfolioDate = get_post_meta($post->ID, 'tj_portfolio_date', true); 
$portfolioURL = get_post_meta($post->ID, 'tj_portfolio_url', true);
$tags = get_the_term_list( $post->ID, 'portfolio-type', '&#35;', ' &#35;', '' );
?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
			
		<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
			<!-- BEGIN Article-->
			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>> 
			
				<div class="entry-content">
				
					<h1 class="entry-title"><?php the_title(); ?></h1>
					
					<div class="seperator"></div>
			
					<?php the_content(); ?>
					
					<div class="post-meta">
							
					<?php
			    	if( !empty($portfolioClient) ) {
			    		$output = '' . __('Client: ','framework') . '<strong>' . $portfolioClient . '</strong> &dash; ';
			    	}
			
			        if( !empty($portfolioDate) ) {
			            $output .= '' . __('Date: ','framework') . '<strong>' . $portfolioDate . '</strong> &dash; ';
			        }
			        
			        if( !empty($portfolioClient) ) {
			        	if( !empty($portfolioURL) ) {
			        		$output .= '<span class="portfolio-more"><a href="' . $portfolioURL . '" target="_blank">'.__('View &rarr;').'</a></span>';
			        	}
			        }
			                        
			        if ( !empty($output) ) {
			        	echo '<p>' . $output . '</p>';
			       	}
			       	
			       	if( !empty($tags) ) { 
			       		echo '<p class="tags"><strong>' . $tags . '</strong></p>';
			       		}  
					?>
							
					</div>

				</div>
				
				<div class="portfolio-media">
				
					<div class="pagination-portfolio clearfix"> 
					
					<?php if( get_previous_post() ) : ?>
						 <div class="pagination-portfolio-left"><p><?php previous_post_link('&larr;</i> %link') ?></p></div>
					<?php endif; ?>
						 			
					<?php if( get_next_post() ) : ?>
						 <div class="pagination-portfolio-right"><p><?php next_post_link('%link &rarr;') ?></p></div>
					<?php endif; ?>
					
					</div>
				
				<?php					
				 	// Get Portfolio Type
				 	$mediaType = get_post_meta($post->ID, 'tj_portfolio_type', true);
				 	
				 	switch( $mediaType ) {
				                        
				        case "Image":
				        tj_image($post->ID, 'project-full');
				        break;
				
				        case "Slideshow":
				        tj_gallery($post->ID, 'project-full');
				        break;
				
						case "Video":
						$embed = get_post_meta($post->ID, 'tj_portfolio_embed_code', true);
						if( !empty($embed) ) {
							echo "<div class='video'>";
						    echo stripslashes(htmlspecialchars_decode($embed));
						    echo "</div>";
						} else {
						 	$type = get_post_type( $post->ID );
						    if ($type === "portfolio") {
						    	tj_video($post->ID, 940, $type);
						    }
						}
						break;
				
				        case "Audio":
				        $type = get_post_type( $post->ID );
				        tj_audio($post->ID, 940, $type);
				        break;
				
				        default:
				        break;
				         
				    }
				?>
				</div>
								
			<!-- END Article -->
			</article>
			
					
	<?php endwhile; endif; ?>
	
	<?php 
	if ( of_get_option('show_related_projects') == '1' ) {
		get_template_part( 'includes/portfolio-recent-projects' );
	} 
	?>
	
	</div>
			
</div>
	
<?php get_footer(); ?>