<?php
/*
Template Name: Portfolio - Paginated
*/
?>
<?php get_header(); ?>

<div id="content" class="portfolio-paginated clearfix">

	<?php tj_portfolio_welcome(); ?>
		
	<?php
	//Posts per page
	$tj_project_count = of_get_option('project_count');
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
	$args = array(
	'post_type' => 'portfolio',
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'posts_per_page' => $tj_project_count,
	'paged' => $paged
	);
	$wp_query = new WP_Query( $args );
	
	$countposts = 0;
	while ( $wp_query->have_posts() ) : $wp_query->the_post(); $countposts++; ?>
			
	<!-- BEGIN Article-->
	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
	
		<div class="mosaic-block fade">
		
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="mosaic-overlay">
				
				<div class="details">
					
					<h3 class="entry-title"><?php the_title(); ?></h3>
					
				</div>
				
			</a>
			
			<div class="mosaic-backdrop">
			
				<?php the_post_thumbnail('project-half'); ?>
				
			</div>
			
		</div>

	<!-- END Article -->
	</article>	
			
	<?php endwhile; wp_reset_postdata(); ?>
		
	<div class="pagination-default clearfix">
	
		<nav>
		
			<p class="pagination-default-left"><?php next_posts_link( __( '&larr; Older Projects', 'framework' ) ); ?></p>
			
			<p class="pagination-default-right"><?php previous_posts_link( __( 'Newer Projects &rarr;', 'framework' ) ); ?></p>
			
		</nav>
		
	</div>
	
	<?php 
	if(of_get_option('show_testimonails') =='1' ){ 
		get_template_part( 'includes/home-recent-testimonials' ); 
	} 
	?>

</div>
	
<?php get_footer(); ?>