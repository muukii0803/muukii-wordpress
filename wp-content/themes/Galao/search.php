<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">


		<h1 class="entry-title"><?php printf( __('Search Results for: &ldquo;%s&rdquo;', 'framework'), get_search_query()); ?></h1>
	
	<?php 
	global $query_string;
	query_posts( $query_string . '&posts_per_page=-1' ); $countposts = 0; if( have_posts() ) : $countposts++; ?>
	
	<!-- BEGIN .post-match -->
	<div class="post-match">
		    		
	 <?php 
	 // return posts only
	 $i = 0;
	 while( have_posts() ) : the_post(); 
	 if( $post->post_type == 'post' ) {
	 $i++;?>
	 
		<!--BEGIN Article -->
		<article id="post-<?php the_ID(); ?>" <?php if ($countposts == 1) { post_class('first clearfix');} else{ post_class('clearfix'); } ?>>
									    						
			<div class="entry-content">
			
			<?php 
				$format = get_post_format();
				if( false === $format ) { $format = 'standard'; }
				get_template_part( 'includes/' . $format ); 
			?>
			
				<?php the_excerpt(); ?>

			</div>
			
			<?php if( $format == 'standard' || $format == 'gallery' || $format == 'image' || $format == 'video' || $format == 'audio') { ?>
			
			<div class="post-meta">
				<p><?php _e('Posted: ','framework'); ?><?php the_time(get_option('date_format')); ?> <?php _e('By: ','framework'); ?> <?php the_author(); ?></p>
				
				<p><?php _e('With: ','framework'); ?><a href="<?php comments_link(); ?>"><?php comments_number( '' . __('0 Comments', 'framework') . '', '' . __('1 Comment', 'framework') . '', '' . __('% Comments', 'framework') . '' ); ?></a></p>
				
			   <?php the_tags('<p class="tags">&#35;',' &#35;', '</p>' ); ?>
			   
			</div>
			
			<?php } ?>
		
		<!-- END Article -->
		</article>
		
	<?php	
	}
	endwhile;
	if( $i == 0 ) { printf('<h3><em>%s</em></h3>', __('No posts match the search terms', 'framework')); } ?>
	    
	<!-- END .post-match -->
	</div>
	
	<?php else : ?>
		
		<h2><?php printf( __('The search for <em>"%s"</em> returned 0 results.','framework'), get_search_query() ); ?></h2>
			
	<?php endif; ?>

	</div>
	
	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>