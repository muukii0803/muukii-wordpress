<?php
/*
Template Name: Page - Full Width
*/
?>
<?php get_header(); ?>
	
<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
		
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<!-- BEGIN Article-->
		<article id="page-<?php the_ID(); ?>" <?php post_class('page-full'); ?>>
		
			<h1 class="entry-title"><?php the_title(); ?></h1>
				
			<div class="entry-content">
			
				<?php the_content(); ?>
			
			</div>
				
			<!--END Article -->
		</article>
	<?php endwhile; endif; ?>
	
	</div>
	
</div>
			
<?php get_footer(); ?>