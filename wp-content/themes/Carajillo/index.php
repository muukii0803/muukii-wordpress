<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
								            
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
		<?php 
		$format = get_post_format();
		if( false === $format ) { $format = 'standard'; }
		get_template_part( 'content', $format ); 
		
		if( ($wp_query->current_post + 1) < ($wp_query->post_count) ) { ?>
		
			<hr class="post-seperator">
			
		<?php } ?>

	<?php endwhile; ?>
				
	<?php else : ?>
	
		<?php get_template_part( 'content', 'none' ); ?>
		
		<hr class="post-seperator">
				
	<?php endif; ?>
	
	</div>
	
	<?php tj_pagination(); ?>
	
</div>

<?php get_footer(); ?>