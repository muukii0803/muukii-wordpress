<form method="get" id="searchform" action="<?php echo home_url(); ?>/">

	<fieldset>
	
		<input type="text" name="s" id="s" value="<?php _e('Search ...', 'framework') ?>" onfocus="if(this.value=='<?php _e('Search ...', 'framework') ?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e('Search ...', 'framework') ?>';" />
		
		<?php 
		
		if ( is_front_page() && is_page_template( 'template-home.php' ) ) { ?>
			
			<input type="hidden" name="post_type" value="gallery" />
		
		<?php }else { ?>
		
			<input type="hidden" name="post_type" value="post" />
		
		<?php } ?>
		
	</fieldset>
	
</form>