<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">

		<article id="post-0" class="first clearfix">		
		
			<div class="entry-content">				
				
				<h2 class="entry-title"><?php _e('You hit a 404!', 'framework') ?></h2>
				
				<p><?php _e( 'Woops, you are looking for something that does exist.', 'framework' ); ?></p>
				
			</div>
	
		</article>
	
	</div>
	
	<?php get_sidebar(); ?>
		
</div>
			
<?php get_footer(); ?>