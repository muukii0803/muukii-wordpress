<div class="footer-wrap">

<footer id="footer" class="clearfix">

	<?php 
	wp_reset_query();
	if ( !(is_page_template('template-home.php')) ) {
		get_sidebar('footer');
	}
	 
	?>
		
	<div class="footer-inner clearfix">
	
		<div class="footer-inner-wrap">
			
			<?php
			if ( of_get_option('footer_left') ) { ?>
			
				<p><?php echo do_shortcode( of_get_option('footer_left') ); ?></p>	
				
			<?php }else { ?>
			
				<p>&copy; <?php _e('Copyright','framework'); ?>  <a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>. <span class="tj-credit"><a href="http://themejug.com/theme/carajillo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - Premium WordPress Theme"><?php bloginfo( 'name' ); ?> Theme</a> <em>by</em> <a href="http://themejug.com" title="ThemeJug - Premium WordPress Themes">ThemeJug</a></span>.</p>
			
			<?php } ?>
			
		</div>
		
	</div>
	
</footer> 

</div>

<?php wp_footer(); ?>
</body>
</html>