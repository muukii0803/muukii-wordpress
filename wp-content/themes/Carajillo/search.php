<?php get_header(); ?>

<div id="content" class="clearfix">

	<div id="primary" class="clearfix">
	
		<div class="search-query">

			<h2 class="search-title meta"><?php printf( __('Search Results for: &ldquo;%s&rdquo;', 'framework'), get_search_query()); ?></h2>
			
		</div>
	
		<?php 
		global $query_string;
		query_posts( $query_string . '&posts_per_page=-1' ); if( have_posts() ) : ?>
		
			<div class="post-match clearfix">
				    		
				<?php
				// display posts
				$i = 0;
				while( have_posts() ) : the_post(); 
				if( $post->post_type == 'post' ) { $i++; echo $post->ID; ?>
				 
				 	<?php 
				 	$format = get_post_format();
				 	if( false === $format ) { $format = 'standard'; }
				 	get_template_part( 'content', $format ); 
				 	if( ($wp_query->current_post + 1) < ($wp_query->post_count) ) { ?>
				 	
				 		<hr class="post-seperator">
				 		
				 	<?php } ?>
							
				<?php 
				}
				endwhile;
				if( $i == 0 ) { printf('<h2 class="search-title meta"><strong>%s</strong></h2>', __('No posts match the search terms', 'framework')); } ?>
			    
			</div>
		
		<?php else : ?>
		
		<article id="post-0" <?php post_class('post'); ?>>
		
			<div class="entry-content">
			
				<p class="search-result-query"><?php printf( __('The search for <em>"%s"</em> returned 0 results.','framework'), get_search_query() ); ?></p>
				
				<h3 class="search-again"><?php _e('Search Again:','framework'); ?></h3>
				
				<div class="no-results-search">
				
					<?php get_search_form(); ?>
					
				</div>
			
			</div>
			
		</article>
				
		<?php endif; ?>
	
		</div>

	</div>

<?php get_footer(); ?>