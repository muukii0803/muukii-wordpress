<div class="footer-widgets clearfix">
	
	<div class="footer-widget-left">
		
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
		
	</div>
	
	<div class="footer-widget-right">
			
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
		
	</div>

</div>