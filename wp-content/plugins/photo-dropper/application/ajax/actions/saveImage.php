<?php
/**
 * Save an Image from PhotoDropper
 * @package PhotoDropper
 * @author Nicky Hajal
 */
if(!class_exists("Pdr_Ajax_Action_SaveImage")){
	class Pdr_Ajax_Action_SaveImage extends Pdr_AjaxAction{

		/**
		 * Add an Image from Flickr
		 * @todo What are the required fields when posting?
		 */
		public function action()
		{
			global $PDR_UTIL;
			$p = (object)$this->p['photo'];
			$url = $p->url;
			$title = $p->title;
			$source_type = $p->type;
			if ($source_type == 'fk') {
				$ext = end(explode('.', str_replace('?zz=1', '', $url)));
			}
			else {
				$ext = 'jpg';
			}
			$updir = wp_upload_dir();
			$dir = $updir['path'];
			$errmsg = false;
			if (!file_exists($dir)) {
				mkdir($dir);
			}
			$slug = str_replace(' ', '_', preg_replace('/[^a-zA-Z0-9\s]/', '', strtolower(trim($title))));
			$image = $PDR_UTIL->get($url);
			if (!$errmsg) {
				$imagefile = $dir . '/' . $slug . '.' . $ext;
				$imageurl = $updir['url'] . '/' . $slug . '.' . $ext;
				file_put_contents($imagefile, $image);
				chmod($imagefile, 0777);

				$url = $imageurl;
				$size = getimagesize($imagefile);
				$type = image_type_to_mime_type($size[2]);
				$file = $imagefile;
				$content = '';

				// use image exif/iptc data for title and caption defaults if possible
				if ( $image_meta = @wp_read_image_metadata($file) ) {
					if ( trim( $image_meta['title'] ) && ! is_numeric( sanitize_title( $image_meta['title'] ) ) )
						$title = $image_meta['title'];
					if ( trim( $image_meta['caption'] ) )
						$content = $image_meta['caption'];
				}

				$post_id = 0;
				$post_data = array();
				$content = '';
				// Construct the attachment array
				$attachment = array_merge( array(
					'post_mime_type' => $type,
					'guid' => $url,
					'post_parent' => $post_id,
					'post_title' => $title,
					'post_content' => $content,
				), $post_data );

				// This should never be set as it would then overwrite an existing attachment.
				if ( isset( $attachment['ID'] ) )
					unset( $attachment['ID'] );

				// Save the data
				$id = wp_insert_attachment($attachment, $file, $post_id);
				$attach_data = wp_generate_attachment_metadata( $id, $file );
				wp_update_attachment_metadata( $id,  $attach_data );
				if ( !is_wp_error($id) && $source_type == 'fk') {
					wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );
					$meta = wp_get_attachment_metadata($id);
					$params = 'user_id=' . $p->owner . '&method=flickr.people.getInfo&format=json&api_key=' . PDR_FKEY;
					$request = 'http://api.flickr.com/services/rest?' . $params;
					$rsp = trim(str_replace('jsonFlickrApi', '', $PDR_UTIL->get($request)), '()');
					$owner = json_decode($rsp)->person;
					$meta['attributeTo'] = $owner->username->_content;
					$meta['attributeOwnerUrl'] = $owner->profileurl->_content;
					$meta['attributeUrl'] = $p->flickrUrl;
					wp_update_attachment_metadata($id, $meta);
				}
				$this->rsp = array("path" => $imagefile, 'mediaid' => $id);
			}
			if ($errmsg) {
				$this->rsp = array("errmsg" => $errmsg);
			}
		}
	}
}

